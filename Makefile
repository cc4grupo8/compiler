make: scanner\Decaf.java DecafScannerCopy.tokens scanner\Decaf.class scanner\Scanner.class parser\Decaf.java DecafParserCopy.tokens DecafParserDel.tokens parser\Decaf.class parser\CC4Parser.class ast\Ast.class semantic\Semantic.class semantic\SemanticAux.class irt\Irt.class codegen\Codegen.class opt\Algebraic.class opt\ConstantFolding.class lib\Debug.class lib\ErrorHandler.class Compiler.class

Compiler.class: Compiler.java
	javac Compiler.java

scanner\Decaf.java: scanner\Decaf.g
	java org.antlr.Tool scanner\Decaf.g

scanner\Decaf.class: scanner\Decaf.java
	javac scanner\Decaf.java

scanner\Scanner.class: scanner\Scanner.java
	javac scanner\Scanner.java

DecafScannerCopy.tokens:
	copy Decaf.tokens scanner

DecafScannerDel.tokens:
	del Decaf.tokens

parser\Decaf.java: parser\Decaf.g
	java org.antlr.Tool parser\Decaf.g

parser\Decaf.class: parser\Decaf.java
	javac parser\Decaf.java

parser\CC4Parser.class: parser\CC4Parser.java
	javac parser\CC4Parser.java

DecafParserCopy.tokens:
	copy Decaf.tokens parser

DecafParserDel.tokens:
	del Decaf.tokens

ast\Ast.class: ast\Ast.java
	javac ast\Ast.java

semantic\Semantic.class: semantic\Semantic.java
	javac semantic\Semantic.java

semantic\SemanticAux.class: semantic\SemanticAux.java
	javac semantic\SemanticAux.java

irt\Irt.class: irt\Irt.java
	javac irt\Irt.java

codegen\Codegen.class: codegen\Codegen.java
	javac codegen\Codegen.java

opt\Algebraic.class: opt\Algebraic.java
	javac opt\Algebraic.java

opt\ConstantFolding.class: opt\ConstantFolding.java
	javac opt\ConstantFolding.java

lib\Debug.class:lib\Debug.java
	javac lib\Debug.java

lib\ErrorHandler.class: lib\ErrorHandler.java
	javac lib\ErrorHandler.java

clean:
	del scanner\*.class scanner\Decaf.java scanner\Decaf.tokens
	del parser\*.class parser\Decaf.java parser\Decaf.tokens
	del codegen\Codegen.class 
	del ast\Ast.class
	del irt\Irt.class 
	del lib\ErrorHandler.class lib\Debug.class 
	del opt\Algebraic.class opt\ConstantFolding.class 
	del semantic\*.class
	del Compiler.class
	del *.s *.dot

