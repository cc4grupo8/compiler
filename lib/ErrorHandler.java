package compiler.lib;

public class ErrorHandler
{
	public void erroresCompiler(int n)
	{
		if(n == 1)
		{
			System.err.println("Error 1: NO EXISTE ARCHIVO, NO SE CONTINUO COMPILANDO.");
		}
		else if(n == 2)
		{
			System.err.println("Error 2: NO SE PUDO CREAR EL ARCHIVO DE SALIDA.");
		}
		else if(n == 3)
		{
			System.err.println("Error 3: ARGUMENTOS NO VALIDOS.");
		}
		else if(n == 4)
		{
			System.err.println("Error 4: NO INGRESO ARGUMENTOS.");	
		}
		else if(n == 5)
		{
			System.err.println("Error 5: NO SE PUDO ESCRIBIR EN EL ARCHIVO.");	
		}
	}

	public void erroresScanner(int n)
	{
		
	}

	public void erroresParser(int n)
	{
		
	}

	public void erroresAst()
	{
		System.err.println("Error 1 AST: NO SE PUDO GENERAR ARBOL.");
	}

	public void erroresSemantic(int n, String str)
	{
		if(n == 1)
		{
			System.err.println("Error 1: ARRAY OUT OF BOUNDS.");
		}
		else if(n == 2)
		{
			System.err.println("Error 2: PROGRAMA NO VALIDO.");
		}
		else if(n == 3)
		{
			System.err.println("Error 3: VARIABLE: \'" + str + "\' YA ESTA DECLARADA");
		}
		else if(n == 4)
		{
			System.err.println("Error 4: ARRAY: \'" + str + "\' YA ESTA DECLARADA");
		}
		else if(n == 5)
		{
			System.err.println("Error 5: TIPO DE VARIABLE: \'" + str + "\' INVALIDO");
		}
		else if(n == 6)
		{
			System.err.println("Error 6: EXPRESIONES EN CICLO FOR INVALIDAS");
		}
		else if(n == 7)
		{
			System.err.println("Error 7: LLAMADA A METODO: \'" + str + "\' INVALIDA, METODO NO EXISTE");
		}
		else if(n == 8)
		{
			System.err.println("Error 8: LLAMADA A METODO: \'" + str + "\' INVALIDA, CANTIDAD DE PARAMETROS INVALIDA");
		}
		else if(n == 9)
		{
			System.err.println("Error 9: LLAMADA A METODO: \'" + str + "\' INVALIDA, EXPRESION INVALIDA");
		}
		else if(n == 10)
		{
			System.err.println("Error 10: LLAMADA A METODO: \'" + str + "\' INVALIDA, TIPO(S) EN LLAMADA INVALIDA");
		}
		else if(n == 11)
		{
			System.err.println("Error 11: EXPRESION EN RETURN INVALIDA");
		}
		else if(n == 12)
		{
			System.err.println("Error 12: TIPO DE EXPRESION EN RETURN INVALIDA");
		}
		else if(n == 13)
		{
			System.err.println("Error 13: RETURN INVALIDO, PARA METODOS DE TIPO VOID, \"return;\"");
		}
		else if(n == 14)
		{
			System.err.println("Error 14: EXPRESION QUE SE DESEA GUARDAR EN LOCATION PARA VARIABLE: " + str + ", INVALIDA, NO SON DEL MISMO TIPO");
		}
		else if(n == 15)
		{
			System.err.println("Error 15: EXPRESION QUE SE DESEA GUARDAR EN LOCATION PARA VARIABLE: " + str + ", INVALIDA");
		}
		else if(n == 16)
		{
			System.err.println("Error 16: VARIABLE: " + str + ", EN LOCATION NO EXISTE");
		}
		else if(n == 17)
		{
			System.err.println("Error 17: EXPRESION DE LA POSICION EN ARRAY QUE SE DESEA GUARDAR EN LA VARIABLE: " + str + ", EN LOCATION ES INVALIDA");
		}
	}
	
}