package compiler.lib;

import compiler.scanner.*;
import compiler.parser.*;
import compiler.ast.*;
import compiler.semantic.*;
import compiler.irt.*;
import compiler.codegen.*;
import java.io.*;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import org.antlr.stringtemplate.*;

public class Debug
{
	public String archivo = "";
	ErrorHandler errores;

	public Debug(String archivo)
    {
        this.archivo = archivo;
        errores = new ErrorHandler();
    }

	public void printScan() 
	{
		try
	    {
			System.out.println("Debugging: Scanner");
			compiler.scanner.Decaf lexerDebug = new compiler.scanner.Decaf(new ANTLRFileStream(this.archivo));
			compiler.scanner.Decaf lexerDebugCopy = new compiler.scanner.Decaf(new ANTLRFileStream(this.archivo));
			while(lexerDebug.nextToken().getType() != Token.EOF)
        	{
	            switch(lexerDebugCopy.nextToken().getType())
	            {
	                case 4:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: BOOLEAN");
	                    break;

	                case 5:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: BREAK");
	                    break;

	                case 6:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: CALLOUT");
	                    break;
	                
	                case 7:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: CLASS");
	                    break;
	                
	                case 8:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COLON");
	                    break;
	                
	                case 9:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COMA");
	                    break;
	                
	                case 10:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COMILLA");
	                    break;
	                
	                case 11:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COMILLAS");
	                    break;
	                
	                case 12:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COMMENT MULTI-LINE");
	                    break;
	                
	                case 13:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COMMENT LINE");
	                    break;
	                
	                case 14:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COND_AND");
	                    break;
	                
	                case 15:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COND_NOT");
	                    break;
	                
	                case 16:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COND_OR");
	                    break;
	                
	                case 17:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: COND_XOR");
	                    break;
	                
	                case 18:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: CONTINUE");
	                    break;
	                
	                case 19:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: DIVISION");
	                    break;
	                
	                case 20:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: ELSE");
	                    break;
	                
	                case 21:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: EQUAL");
	                    break;
	                
	                case 22:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: EQUAL_EQUAL");
	                    break;
	                
	                case 23:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: ERR1_LITERAL_CHAR");
	                    break;
	                
	                case 24:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: ERR2_LITERAL_CHAR");
	                    break;
	                
	                case 25:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: ERR3_LITERAL_CHAR");
	                    break;
	                
	                case 26:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: ERR4_LITERAL_CHAR");
	                    break;
	                
	                case 27:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: ERR5_LITERAL_CHAR");
	                    break;
	                
	                case 28:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: EROR_LITERAL_HEXA");
	                    break;
	                
	                case 30:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: FLOTANTES");
	                    break;
	                
	                case 31:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: FOR");
	                    break;
	                
	                case 32:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: IF");
	                    break;
	                
	                case 33:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: INT");
	                    break;
	                
	                case 34:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LBRACKET");
	                    break;
	                
	                case 35:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LITERAL_BOOL");
	                    break;
	                
	                case 36:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LITERAL_CHAR");
	                    break;
	                
	                case 37:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LITERAL_ENTEROS");
	                    break;
	                
	                case 38:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LITERAL_HEXA");
	                    break;

	                case 39:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LITERAL_STRING");
	                    break;

	                case 40:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LPARENTESIS");
	                    break;
	                    
	                case 41:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: LSQUIGLY");
	                    break;
	                    
	                case 42:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: MINUS_EQUAL");
	                    break;
	                    
	                case 43:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: MULTIPLICACION");
	                    break;
	                    
	                case 44:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: NOT_EQUAL");
	                    break;
	                    
	                case 45:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: PLUS_EQUAL");
	                    break;
	                    
	                case 46:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: PROGRAM");
	                    break;
	                    
	                case 47:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: PUNTO");
	                    break;
	                    
	                case 48:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: PUNTO Y COMA");
	                    break;
	               
	                case 49:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: RBRACKET");
	                    break;
	                    
	                case 50:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: REL_MAYOR");
	                    break;
	                    
	                case 51:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: REL_MAYOREQ");
	                    break;
	                    
	                case 52:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: REL_MENOR");
	                    break;
	                    
	                case 53:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: REL_MENOREQ");
	                    break;
	                    
	                case 54:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: REMAINDER");
	                    break;
	                    
	                case 55:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: RESTA");
	                    break;
	                    
	                case 56:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: RETURN");
	                    break;
	                    
	                case 57:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: RPARENTESIS");
	                    break;
	                    
	                case 58:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: RSQUIGLY");
	                    break;
	                    
	                case 59:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: SUMA");
	                    break;
	                    
	                case 60:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: VARIABLES");
	                    break;
	                    
	                case 61:
	                    System.out.println("\t Line " + lexerDebugCopy.getLine() + ":" + lexerDebugCopy.getCharPositionInLine() + "\t Found: \'" + lexerDebugCopy.getText() + "\'\t Token: VOID");
	                    break;
	            }
	        }
	    }
	    catch (Exception e)
	    {
	    	errores.erroresCompiler(0);
	    	System.exit(0);
	    }
    }

	public void printParser()
	{
		try
	    {
			System.out.println("\nDebugging: Parser");
			ANTLRFileStream entrada = new ANTLRFileStream(this.archivo);
		    compiler.scanner.Decaf lexer = new compiler.scanner.Decaf(entrada);
		    CommonTokenStream tokens = new CommonTokenStream(lexer);
		    compiler.parser.Decaf parser = new compiler.parser.Decaf(tokens);
		    parser.start();
		    for(int i = parser.lista.size()-1; i >= 0; i--)
		    {
		    	System.out.println(parser.lista.get(i));
		    }
		}
	    catch (Exception e)
	    {
	    	
	    	errores.erroresCompiler(0);
	    	System.exit(0);
	    }
	}

	public void printAst()
	{
		try
	    {
			System.out.print("\nDebugging: Ast");
			ANTLRFileStream entrada = new ANTLRFileStream(this.archivo);
            compiler.scanner.Decaf lexer = new compiler.scanner.Decaf(entrada);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            compiler.parser.Decaf parser = new compiler.parser.Decaf(tokens);
            compiler.parser.Decaf.start_return r = parser.start();

			Tree t = (Tree)r.getTree();
			
			//Imprimir "bonito"
			/*String tPrintOriginal = new String(t.toStringTree());
			String tPrintNew = "";
			String tabLines = "\n-~-";

			for(int x = 0; x < tPrintOriginal.length()-2; x++)
			{
				if(tPrintOriginal.substring(x, x + 1).equals("("))
				{
					tPrintNew = tPrintNew +  tabLines + "> ";
					x++;
					tabLines = tabLines + "-~-";
				}
				else if(tPrintOriginal.substring(x, x + 1).equals(")"))
				{
					x++;
					tabLines = tabLines.substring(0, tabLines.length()-3);
					if(tPrintOriginal.substring(x, x + 1).equals(")"))
					{
						x++;
						tabLines = tabLines.substring(0, tabLines.length()-3);
						if(tPrintOriginal.substring(x, x + 1).equals(")"))
						{
							x++;
							tabLines = tabLines.substring(0, tabLines.length()-3);
						}
					}
				}
				if(!(tPrintOriginal.substring(x, x + 1).equals(")")))
				{
					tPrintNew = tPrintNew + tPrintOriginal.charAt(x);
				}
			}*/
			System.out.println(t.toStringTree());


			DOTTreeGenerator gen = new DOTTreeGenerator();
			StringTemplate st = gen.toDOT(t);
			PrintStream ps = new PrintStream(this.archivo + ".dot");
			ps.println(st);
		}
	    catch (Exception e)
	    {
	    	
	    	errores.erroresCompiler(0);
	    	System.exit(0);
	    }
	}

	public void printSemantic()
	{
		try
		{
			System.out.println("Debugging: Semantic");
			compiler.scanner.Scanner scan = new compiler.scanner.Scanner(this.archivo);
			compiler.parser.CC4Parser parse = new compiler.parser.CC4Parser(scan);
			compiler.ast.Ast ast = new compiler.ast.Ast(parse);
			compiler.semantic.Semantic semantica = new compiler.semantic.Semantic(ast);
			semantica.printScreen();
		}
	    catch (Exception e)
	    {
	    	errores.erroresCompiler(1);
	    	e.printStackTrace();
	    	System.exit(0);
	    }
	}

	public void printIrt()
	{
		System.out.println("Debugging: Irt");
	}

	public void printCodegen()
	{
		System.out.println("Debugging: Codegen");
	}
}
