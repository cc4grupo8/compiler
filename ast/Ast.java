package compiler.ast;

import compiler.scanner.Scanner;
import compiler.parser.CC4Parser;
import compiler.lib.ErrorHandler;
import java.io.*;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import org.antlr.stringtemplate.*;

public class Ast
{
    public String archivo = "";
	ErrorHandler errores;

    public Ast(CC4Parser parser)
    {
        this.archivo = parser.archivo;
        errores = new ErrorHandler();
    }

    public void print(FileWriter fw, PrintWriter pw)
    {
    	try 
    	{
    		pw.print("\nStage: ast");
    		pw.flush();	    
			
            ANTLRFileStream entrada = new ANTLRFileStream(this.archivo);
            compiler.scanner.Decaf lexer = new compiler.scanner.Decaf(entrada);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            compiler.parser.Decaf parser = new compiler.parser.Decaf(tokens);
            compiler.parser.Decaf.start_return r = parser.start();

			Tree t = (Tree)r.getTree();
			
            //Imprimir "bonito"
            /*String tPrintOriginal = new String(t.toStringTree());
            String tPrintNew = "";
            String tabLines = "\n-~-";

            for(int x = 0; x < tPrintOriginal.length()-2; x++)
            {
                if(tPrintOriginal.substring(x, x + 1).equals("("))
                {
                    tPrintNew = tPrintNew +  tabLines + "> ";
                    x++;
                    tabLines = tabLines + "-~-";
                }
                else if(tPrintOriginal.substring(x, x + 1).equals(")"))
                {
                    x++;
                    tabLines = tabLines.substring(0, tabLines.length()-3);
                    if(tPrintOriginal.substring(x, x + 1).equals(")"))
                    {
                        x++;
                        tabLines = tabLines.substring(0, tabLines.length()-3);
                        if(tPrintOriginal.substring(x, x + 1).equals(")"))
                        {
                            x++;
                            tabLines = tabLines.substring(0, tabLines.length()-3);
                        }
                    }
                }
                if(!(tPrintOriginal.substring(x, x + 1).equals(")")))
                {
                    tPrintNew = tPrintNew + tPrintOriginal.charAt(x);
                }
            }
            pw.println(tPrintNew);
			pw.flush();*/

            pw.println(t.toStringTree());
            pw.flush();
			
			DOTTreeGenerator gen = new DOTTreeGenerator();
			StringTemplate st = gen.toDOT(t);
			PrintStream ps = new PrintStream(this.archivo + ".dot");
			ps.println(st);
	    } 
	    catch (Exception e) 
    	{
    		errores.erroresAst();
            e.printStackTrace();
      		System.exit(0);
    	} 
  	}
}
