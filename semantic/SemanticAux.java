package compiler.semantic;

import java.util.*;
import compiler.lib.*;

public class SemanticAux
{
	public String type;
	public String methodParameters;
	public boolean boolParameter;
	public int intParameter;
	public int parentTable;
	public int numTable;
	public int size;
	public int offset;
	public int boolVarOrMethod;
	public LinkedList<Integer> intList;
	public LinkedList<Boolean> boolList;
	public ErrorHandler errores;

	//Constructor para "int"
	public SemanticAux(String type, int intParameter, int offset, int numTable)
	{
		this.type = type;
		this.intParameter = intParameter;
		this.size = 0;
		this.offset = offset;
		this.numTable = numTable;
		this.boolVarOrMethod = 0;
		errores = new ErrorHandler();
	}
	
	//Constructor para "boolean"
	public SemanticAux(String type, boolean boolParameter, int offset, int numTable)
	{
		this.type = type;
		this.boolParameter = boolParameter;
		this.size = 0;
		this.offset = offset;
		this.numTable = numTable;
		this.boolVarOrMethod = 0;
		errores = new ErrorHandler();
	}

	//Constructor para "arrays" (Solo en Scope Global)
	public SemanticAux(String type, int size, int arrayIntParameter, boolean arrayBoolParameter, int offset, int numTable)
	{
		this.type = type;
		this.size = size;
		this.offset = offset;
		this.numTable = numTable;
		this.boolVarOrMethod = 0;
		intList = new LinkedList<Integer>();
		boolList = new LinkedList<Boolean>();
		errores = new ErrorHandler();
		if(this.type.equals("int"))
		{
			for(int i = 0; i < size; i++)
			{
				intList.add(arrayIntParameter);
			}
		}
		else
		{
			for(int i = 0; i < size; i++)
			{
				boolList.add(arrayBoolParameter);
			}
		}
	}

	//Constructor para declaracion de Scopes (metodos, ifs, fors, blocks)
	public SemanticAux(String type, String methodParameters, int parentTable, int numTable)
	{
		this.type = type;
		this.methodParameters = methodParameters;
		this.parentTable = parentTable;
		this.numTable = numTable;
		this.boolVarOrMethod = 1;
	}	

	public String getType()
	{
		return this.type;
	}
	
	public boolean getBoolParameter()
	{
		return this.boolParameter;
	}

	public int getIntParameter()
	{
		return this.intParameter;
	}

	public int getParentTable()
	{
		return this.parentTable;
	}

	public int getNumTable()
	{
		return this.numTable;
	}

	public int getSize()
	{
		return this.size;
	}

	public int getOffset()
	{
		return this.offset;
	}

	public int getBoolVarOrMethod()
	{
		return this.boolVarOrMethod;
	}

	public boolean setIntPosition(int pos, int parameter)
	{
		if(pos < size)
		{
			intList.set(pos, parameter);
			return true;
		}
		else
		{
			errores.erroresSemantic(1, "");
			System.exit(0);
			return false;
		}
	}

	public boolean setBoolPosition(int pos, boolean parameter)
	{
		if(pos < size)
		{
			boolList.set(pos, parameter);
			return true;
		}
		else
		{
			errores.erroresSemantic(1, "");
			System.exit(0);
			return false;
		}
	}

	public int getInt(int pos)
	{
		if(pos < size)
		{
			return intList.get(pos);
		}
		else
		{
			errores.erroresSemantic(1, "");
			System.exit(0);
			return -1;
		}
	}

	public boolean getBool(int pos)
	{
		if(pos < size)
		{
			return boolList.get(pos);
		}
		else
		{
			errores.erroresSemantic(1, "");
			System.exit(0);
			return false;
		}
	}

	public String intToString()
	{
		return " " + this.type + "-> " + this.intParameter + " Offset: " + offset;
	}

	public String boolToString()
	{
		return " " + this.type + "-> " + this.boolParameter + " Offset: " + offset;
	}

	public String intListToString()
	{
		String temp = "\n";
		for(int i = 0; i < this.size; i++)
		{
			temp = temp + "Position: " + i + " " + this.type + "-> " + this.intList.get(i)  + " Offset: " + offset + "\n";
		}
		return temp;
	}

	public String boolListToString()
	{
		String temp = "\n";
		for(int i = 0; i < this.size; i++)
		{
			temp = temp + "Position: " + i + " " + this.type + "-> " + this.boolList.get(i)  + " Offset: " + offset + "\n";
		}
		return temp;
	}

	public String methodParameterToString()
	{
		return " " + this.type + "-> " + this.methodParameters;
	}
}