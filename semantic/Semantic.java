package compiler.semantic;

import compiler.ast.Ast;
import compiler.lib.*;
import java.util.*;
import java.io.*;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class Semantic
{
	public String archivo;
	ErrorHandler errores;
	public Hashtable<String, SemanticAux> tabla = new Hashtable<String, SemanticAux>();
	public LinkedList<Tree> fieldsList = new LinkedList<Tree>();
	public LinkedList<Tree> arraysList = new LinkedList<Tree>();
	public LinkedList<Tree> methodsList = new LinkedList<Tree>();
	public LinkedList<Tree> varsList = new LinkedList<Tree>();
	public LinkedList<Tree> statementList = new LinkedList<Tree>();
	public LinkedList<Hashtable<String, SemanticAux>> hashTableList = new LinkedList<Hashtable<String, SemanticAux>>();
	public LinkedList<LinkedList<String>> hashTableListKeys = new LinkedList<LinkedList<String>>();
	public Tree tree;
	public boolean principalBool;
	public int numTableMethods;
	public int numTableMethodsTemp;
	public int numTableScopes;
	public int offset;
	public String expTypeGlobal;

	public Semantic(Ast ast)
	{
		this.archivo = ast.archivo;
		errores = new ErrorHandler();
		offset = 0;
		numTableMethods = 0;
		numTableMethodsTemp = -1;
		numTableScopes = 0;
	}

	public void print(FileWriter fw, PrintWriter pw)
	{
		try 
		{
			pw.print("\nStage: Semantic");
			pw.flush();	    
			
			ANTLRFileStream entrada = new ANTLRFileStream(this.archivo);
			compiler.scanner.Decaf lexer = new compiler.scanner.Decaf(entrada);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			compiler.parser.Decaf parser = new compiler.parser.Decaf(tokens);
			compiler.parser.Decaf.start_return r = parser.start();

			Tree t = (Tree)r.getTree();
			this.tree = t;
			saveAndFill();
			if(principalBool)
			{
				pw.print(getTable());
				pw.flush();
			}
			else
			{
				errores.erroresSemantic(2, "");
			}
		}
		catch (Exception e) 
		{
			errores.erroresAst();
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void printScreen()
	{
		try 
		{
			ANTLRFileStream entrada = new ANTLRFileStream(this.archivo);
			compiler.scanner.Decaf lexer = new compiler.scanner.Decaf(entrada);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			compiler.parser.Decaf parser = new compiler.parser.Decaf(tokens);
			compiler.parser.Decaf.start_return r = parser.start();

			Tree t = (Tree)r.getTree();
			this.tree = t;
			saveAndFill();
			if(principalBool)
			{
				System.out.println(getTable());
			}
			else
			{
				errores.erroresSemantic(2, "");
			}
		}
		catch (Exception e) 
		{
			errores.erroresAst();
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void saveAndFill()
	{
		principalBool = true;
		//Guardando hijos ARRAY_DEC, FIELD_DEC, METHOD_DECL LinkedList distintas.
		for(int x = 0; x < tree.getChildCount(); x++)
		{
			if(tree.getChild(x).getText().equals("ARRAY_DEC"))
			{
				arraysList.add(tree.getChild(x));
			}
			else if(tree.getChild(x).getText().equals("FIELD_DEC"))
			{
				fieldsList.add(tree.getChild(x));
			}
			else if(tree.getChild(x).getText().equals("METHOD_DECL"))
			{
				methodsList.add(tree.getChild(x));
			}
		}
		if(principalBool)
		{
			//Llenando tabla de simbolos, parametros vacios.
			for(int x = 0; x < fieldsList.size(); x++)
			{
				String typeTemp = fieldsList.get(x).getChild(0).getText();
				for(int y = 1; y < fieldsList.get(x).getChildCount(); y++)
				{
					String keyTemp = fieldsList.get(x).getChild(y).getText();
					//Chequeo unicidad (que no se repita ningun key)
					if(tabla.containsKey(keyTemp))
					{
						principalBool = false;
						errores.erroresSemantic(3, keyTemp);
						System.exit(0);
						break;
					}
					else
					{
						tabla.put(keyTemp, new SemanticAux(typeTemp, 0, offset, -1));
						offset+=4;
					}
				}
			}

			for(int x = 0; x < arraysList.size(); x++)
			{
				String typeTemp = arraysList.get(x).getChild(0).getText();
				for(int y = 1; y < arraysList.get(x).getChildCount(); y+=2)
				{
					String keyTemp = arraysList.get(x).getChild(y).getText();
					String sizeTemp = arraysList.get(x).getChild(y+1).getText();
					//Chequeo unicidad (que no se repita ningun key)
					if(tabla.containsKey(keyTemp))
					{
						principalBool = false;
						errores.erroresSemantic(4, keyTemp);
						System.exit(0);
						break;
					}
					else
					{
						if(sizeTemp.length()>=3)
						{
							if(sizeTemp.substring(0,2).equals("0x") || sizeTemp.substring(0,2).equals("0X"))
							{
								tabla.put(keyTemp, new SemanticAux(typeTemp, Integer.valueOf(sizeTemp.substring(2, sizeTemp.length()),16).intValue(), 0, false, offset, -1));
								offset+=4;
							}
							else
							{
								tabla.put(keyTemp, new SemanticAux(typeTemp, Integer.parseInt(sizeTemp), 0, false, offset, -1));
								offset+=4;
							}
						}
						else
						{
							tabla.put(keyTemp, new SemanticAux(typeTemp, Integer.parseInt(sizeTemp), 0, false, offset, -1));
							offset+=4;
						}
					}
				}
			}

			for(int x = 0; x < methodsList.size(); x++)
			{
				String typeTemp = methodsList.get(x).getChild(0).getText();
				String keyTemp = methodsList.get(x).getChild(1).getText();
				if(methodsList.get(x).getChildCount() == 3)
				{
					Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
					hashTableListKeys.add(new LinkedList<String>());
					Tree blockTemp = methodsList.get(x).getChild(methodsList.get(x).getChildCount()-1);
					LinkedList<Tree> varsList = new LinkedList<Tree>();
					LinkedList<Tree> statementList = new LinkedList<Tree>();

					for(int y = 0; y < blockTemp.getChildCount(); y++)
					{
						if(blockTemp.getChild(y).getText().equals("VARS"))
						{
							varsList.add(blockTemp.getChild(y));
						}
						else if(blockTemp.getChild(y).getText().equals("STATEMENT"))
						{
							statementList.add(blockTemp.getChild(y));
						}
					}
					//Llenando tabla auxiliar de simbolos, parametros vacios.
					for(int y = 0; y < varsList.size(); y++)
					{
						String typeTempOther = varsList.get(y).getChild(0).getText();
						for(int z = 1; z < varsList.get(y).getChildCount(); z++)
						{
							String keyTempOther = varsList.get(y).getChild(z).getText();
							//Chequeo unicidad (que no se repita ningun key)
							if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
							{
								principalBool = false;
								errores.erroresSemantic(3, keyTempOther);
								System.exit(0);
								break;
							}
							else
							{
								tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableMethods));
								LinkedList<String> listTemp = hashTableListKeys.get(x);
								listTemp.add(keyTempOther);
								hashTableListKeys.set(x, listTemp);
								offset+=4;
							}
						}
					}
					//Para todos los metodos su "Parent" en el constructor de SemanticAux sera -1 que es Global Scope
					tabla.put(keyTemp, new SemanticAux(typeTemp, "", -1, numTableMethods));
					hashTableList.add(tablaAux);
					numTableMethods++;
				}
				else
				{
					Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
					hashTableListKeys.add(new LinkedList<String>());
					String methodParametersTemp = "";
					for(int y = 2; y < methodsList.get(x).getChildCount()-1; y++)
					{
						String typeTempAux = methodsList.get(x).getChild(y).getText();
						String keyTempAux = methodsList.get(x).getChild(y).getChild(0).getText();
						if(y < methodsList.get(x).getChildCount()-2)
						{
							methodParametersTemp = methodParametersTemp + typeTempAux + ", ";
						}
						else
						{
							methodParametersTemp = methodParametersTemp + typeTempAux;
						}

						if(typeTempAux.equals("int"))
						{
							if(tablaAux.containsKey(keyTempAux) || tabla.containsKey(keyTempAux))
							{
								principalBool = false;
								errores.erroresSemantic(3, keyTempAux);
								System.exit(0);
							}
							else
							{
								tablaAux.put(keyTempAux, new SemanticAux(typeTempAux, 0, offset, numTableMethods));
								LinkedList<String> listTemp = hashTableListKeys.get(x);
								listTemp.add(keyTempAux);
								hashTableListKeys.set(x, listTemp);
								offset+=4;
							}
						}
						else if(typeTempAux.equals("boolean"))
						{
							if(tablaAux.containsKey(keyTempAux) || tabla.containsKey(keyTempAux))
							{
								principalBool = false;
								errores.erroresSemantic(3, keyTempAux);
								System.exit(0);
							}
							else
							{
								tablaAux.put(keyTempAux, new SemanticAux(typeTempAux, false, offset, numTableMethods));
								LinkedList<String> listTemp = hashTableListKeys.get(x);
								listTemp.add(keyTempAux);
								hashTableListKeys.set(x, listTemp);
								offset+=4;
							}
						}
						else
						{
							principalBool = false;
							errores.erroresSemantic(3, typeTempAux);
							System.exit(0);
						}
					}
					Tree blockTemp = methodsList.get(x).getChild(methodsList.get(x).getChildCount()-1);
					LinkedList<Tree> varsList = new LinkedList<Tree>();
					LinkedList<Tree> statementList = new LinkedList<Tree>();
					for(int y = 0; y < blockTemp.getChildCount(); y++)
					{
						if(blockTemp.getChild(y).getText().equals("VARS"))
						{
							varsList.add(blockTemp.getChild(y));
						}
						else if(blockTemp.getChild(y).getText().equals("STATEMENT"))
						{
							statementList.add(blockTemp.getChild(y));
						}
					}
					//Llenando tabla auxiliar de simbolos, parametros vacios.
					for(int y = 0; y < varsList.size(); y++)
					{
						String typeTempOther = varsList.get(y).getChild(0).getText();
						for(int z = 1; z < varsList.get(y).getChildCount(); z++)
						{
							String keyTempOther = varsList.get(y).getChild(z).getText();
							//Chequeo unicidad (que no se repita ningun key)
							if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
							{
								principalBool = false;
								errores.erroresSemantic(3, keyTempOther);
								System.exit(0);
								break;
							}
							else
							{
								tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableMethods));
								LinkedList<String> listTemp = hashTableListKeys.get(x);
								listTemp.add(keyTempOther);
								hashTableListKeys.set(x, listTemp);
								offset+=4;
							}
						}
					}
					//Para todos los metodos su "Parent" en el constructor de SemanticAux sera -1 que es Global Scope
					tabla.put(keyTemp, new SemanticAux(typeTemp, methodParametersTemp, -1, numTableMethods));
					hashTableList.add(tablaAux);
					numTableMethods++;
				}
			}
			numTableScopes = methodsList.size();
			numTableMethodsTemp = -1;
			//statementList dentro de methosList (Aqui se empieza a llamar a recursion si es necesario)
			for(int x = 0; x < methodsList.size(); x++)
			{
				Tree blockTemp = methodsList.get(x).getChild(methodsList.get(x).getChildCount()-1);
				LinkedList<Tree> varsList = new LinkedList<Tree>();
				LinkedList<Tree> statementList = new LinkedList<Tree>();
				String typeTemp = methodsList.get(x).getChild(0).getText();
				String keyTemp = methodsList.get(x).getChild(1).getText();
				String methodsListTypeAux = methodsList.get(x).getChild(0).getText();
				numTableMethodsTemp++;
				for(int y = 0; y < blockTemp.getChildCount(); y++)
				{
					if(blockTemp.getChild(y).getText().equals("VARS"))
					{
						varsList.add(blockTemp.getChild(y));
					}
					else if(blockTemp.getChild(y).getText().equals("STATEMENT"))
					{
						statementList.add(blockTemp.getChild(y));
					}
					else
					{
						principalBool = false;
					}
				}

				for(int y = 0; y < statementList.size(); y++)
				{
					String statementIni = statementList.get(y).getChild(0).getText();
					if(statementIni.equals("if") && statementList.get(y).getChild(1).getText().equals("EXP") && statementList.get(y).getChild(2).getText().equals("BLOCK"))
					{
						if(checkExpMaster(statementList.get(y).getChild(1)))
						{
							//Para "if"
							if((statementList.get(y).getChildCount() == 3) || (statementList.get(y).getChildCount() == 5))
							{
								Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
								LinkedList<String> hashTableListKeysAux = new LinkedList<String>();
								LinkedList<Tree> varsListAux = new LinkedList<Tree>();
								LinkedList<Tree> statementListAux = new LinkedList<Tree>();
								for(int z = 0; z < statementList.get(y).getChild(2).getChildCount(); z++)
								{
									if(statementList.get(y).getChild(2).getChild(z).getText().equals("VARS"))
									{
										varsListAux.add(statementList.get(y).getChild(2).getChild(z));
									}
									else if(statementList.get(y).getChild(2).getChild(z).getText().equals("STATEMENT"))
									{
										statementListAux.add(statementList.get(y).getChild(2).getChild(z));
									}
								}
								tablaAux.put(keyTemp + "_if_" + numTableScopes, new SemanticAux("if", "", numTableMethodsTemp, numTableScopes));
								hashTableListKeysAux.add(keyTemp + "_if_" + numTableScopes);
								numTableScopes++;
								//Llenando tabla auxiliar de simbolos, parametros vacios.
								for(int k = 0; k < varsListAux.size(); k++)
								{
									String typeTempOther = varsListAux.get(k).getChild(0).getText();
									for(int z = 1; z < varsListAux.get(k).getChildCount(); z++)
									{
										String keyTempOther = varsListAux.get(k).getChild(z).getText();
										//Chequeo unicidad (que no se repita ningun key)
										if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
										{
											principalBool = false;
											errores.erroresSemantic(3, keyTempOther);
											System.exit(0);
											break;
										}
										else
										{
											tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
											hashTableListKeysAux.add(keyTempOther);
											offset+=4;
										}
									}
								}
								hashTableListKeys.add(hashTableListKeysAux);
								hashTableList.add(tablaAux);
								//RECURSION
								recursionStatements(statementListAux, keyTemp + "_if_" + (numTableScopes - 1), methodsListTypeAux);
								//Para "else" (del "if" anterior)
								if(statementList.get(y).getChildCount() == 5)
								{
									Hashtable<String, SemanticAux> tablaAuxOther = new Hashtable<String, SemanticAux>();
									LinkedList<String> hashTableListKeysAuxOther = new LinkedList<String>();
									LinkedList<Tree> varsListAuxOther = new LinkedList<Tree>();
									LinkedList<Tree> statementListAuxOther = new LinkedList<Tree>();
									for(int z = 0; z < statementList.get(y).getChild(4).getChildCount(); z++)
									{
										if(statementList.get(y).getChild(4).getChild(z).getText().equals("VARS"))
										{
											varsListAuxOther.add(statementList.get(y).getChild(4).getChild(z));
										}
										else if(statementList.get(y).getChild(4).getChild(z).getText().equals("STATEMENT"))
										{
											statementListAuxOther.add(statementList.get(y).getChild(4).getChild(z));
										}
									}
									tablaAuxOther.put(keyTemp + "_else_" + numTableScopes, new SemanticAux("else", "", numTableMethodsTemp, numTableScopes));
									hashTableListKeysAuxOther.add(keyTemp + "_else_" + numTableScopes);
									numTableScopes++;
									//Llenando tabla auxiliar de simbolos, parametros vacios.
									for(int k = 0; k < varsListAuxOther.size(); k++)
									{
										String typeTempOther = varsListAuxOther.get(k).getChild(0).getText();
										for(int z = 1; z < varsListAuxOther.get(k).getChildCount(); z++)
										{
											String keyTempOther = varsListAuxOther.get(k).getChild(z).getText();
											//Chequeo unicidad (que no se repita ningun key)
											if(tabla.containsKey(keyTempOther) || tablaAuxOther.containsKey(keyTempOther))
											{
												principalBool = false;
												errores.erroresSemantic(3, keyTempOther);
												System.exit(0);
												break;
											}
											else
											{
												tablaAuxOther.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
												hashTableListKeysAuxOther.add(keyTempOther);
												offset+=4;
											}
										}
									}
									hashTableListKeys.add(hashTableListKeysAuxOther);
									hashTableList.add(tablaAuxOther);
									//RECURSION
									recursionStatements(statementListAuxOther, keyTemp + "_else_" + (numTableScopes - 1), methodsListTypeAux);
								}
							}
							else
							{
								principalBool = false;
							}
						}
						else
						{
							principalBool = false;
						}
						
					}
					else if(statementIni.equals("for") && statementList.get(y).getChild(3).getText().equals("EXP") && statementList.get(y).getChild(4).getText().equals("EXP"))
					{
						boolean boolTempFor = false;
						for(int z = 0; z < hashTableList.size(); z++)
						{
							if(hashTableList.get(z).containsKey(statementList.get(y).getChild(1).getText()))
							{
								boolTempFor = true;
								break;
							}
						}
						if(boolTempFor == false)
						{
							if(tabla.containsKey(statementList.get(y).getChild(1).getText()))
							{
								boolTempFor = true;
							}
						}
						if(checkExpMaster(statementList.get(y).getChild(3)) && checkExpMaster(statementList.get(y).getChild(4)))
						{
							Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
							LinkedList<String> hashTableListKeysAux = new LinkedList<String>();
							LinkedList<Tree> varsListAux = new LinkedList<Tree>();
							LinkedList<Tree> statementListAux = new LinkedList<Tree>();
							for(int z = 0; z < statementList.get(y).getChild(5).getChildCount(); z++)
							{
								if(statementList.get(y).getChild(5).getChild(z).getText().equals("VARS"))
								{
									varsListAux.add(statementList.get(y).getChild(5).getChild(z));
								}
								else if(statementList.get(y).getChild(5).getChild(z).getText().equals("STATEMENT"))
								{
									statementListAux.add(statementList.get(y).getChild(5).getChild(z));
								}
							}
							tablaAux.put(keyTemp + "_for_" + numTableScopes, new SemanticAux("for", "", numTableMethodsTemp, numTableScopes));
							hashTableListKeysAux.add(keyTemp + "_for_" + numTableScopes);
							numTableScopes++;
							//Si variable de inicializacion de for no existe, la creamos. Si ya existe, guardar valor original en otro lugar temporalmente y restaurar al finalizar for.
							if(boolTempFor == false)
							{
								tablaAux.put(statementList.get(y).getChild(1).getText(), new SemanticAux("int", 0, offset, numTableScopes));
								hashTableListKeysAux.add(statementList.get(y).getChild(1).getText());
								offset+=4;
							}
							else
							{
								//Guardar valor original en otro lugar temporalmente y restaurarlo al finalizar for.
							}
							//Llenando tabla auxiliar de simbolos, parametros vacios.
							for(int k = 0; k < varsListAux.size(); k++)
							{
								String typeTempOther = varsListAux.get(k).getChild(0).getText();
								for(int z = 1; z < varsListAux.get(k).getChildCount(); z++)
								{
									String keyTempOther = varsListAux.get(k).getChild(z).getText();
									//Chequeo unicidad (que no se repita ningun key)
									if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
									{
										principalBool = false;
										errores.erroresSemantic(3, keyTempOther);
										System.exit(0);
										break;
									}
									else
									{
										tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
										hashTableListKeysAux.add(keyTempOther);
										offset+=4;
									}
								}
							}
							hashTableListKeys.add(hashTableListKeysAux);
							hashTableList.add(tablaAux);
							//RECURSION
							recursionStatements(statementListAux, keyTemp + "_for_" + (numTableScopes - 1), methodsListTypeAux);
						}
						else
						{
							principalBool = false;
							errores.erroresSemantic(6, statementList.get(y).getChild(1).getText());
							System.exit(0);
						}
					}
					else if(statementIni.equals("BLOCK"))
					{
						Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
						LinkedList<String> hashTableListKeysAux = new LinkedList<String>();
						LinkedList<Tree> varsListAux = new LinkedList<Tree>();
						LinkedList<Tree> statementListAux = new LinkedList<Tree>();
						for(int z = 0; z < statementList.get(y).getChild(0).getChildCount(); z++)
						{
							if(statementList.get(y).getChild(0).getChild(z).getText().equals("VARS"))
							{
								varsListAux.add(statementList.get(y).getChild(0).getChild(z));
							}
							else if(statementList.get(y).getChild(0).getChild(z).getText().equals("STATEMENT"))
							{
								statementListAux.add(statementList.get(y).getChild(0).getChild(z));
							}
						}
						tablaAux.put(keyTemp + "_block_" + numTableScopes, new SemanticAux("block", "", numTableMethodsTemp, numTableScopes));
						hashTableListKeysAux.add(keyTemp + "_block_" + numTableScopes);
						numTableScopes++;
						//Llenando tabla auxiliar de simbolos, parametros vacios.
						for(int k = 0; k < varsListAux.size(); k++)
						{
							String typeTempOther = varsListAux.get(k).getChild(0).getText();
							for(int z = 1; z < varsListAux.get(k).getChildCount(); z++)
							{
								String keyTempOther = varsListAux.get(k).getChild(z).getText();
								//Chequeo unicidad (que no se repita ningun key)
								if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
								{
									principalBool = false;
									errores.erroresSemantic(3, keyTempOther);
									System.exit(0);
									break;
								}
								else
								{
									tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
									hashTableListKeysAux.add(keyTempOther);
									offset+=4;
								}
							}
						}
						hashTableListKeys.add(hashTableListKeysAux);
						hashTableList.add(tablaAux);
						//RECURSION
						recursionStatements(statementListAux, keyTemp + "_block_" + (numTableScopes - 1), methodsListTypeAux);
					}
					else if(statementIni.equals("METHOD_CALL"))
					{
						boolean boolTempMCall = false;
						String mCallName = statementList.get(y).getChild(0).getChild(0).getText();
						Tree mCallTree = null;
						if(mCallName.equals("callout"))
						{
							//ALGO***************************************************************
						}
						else if(mCallName.equals("printInt"))
						{
							//ALGO***************************************************************
						}
						else if(mCallName.equals("printBoolean"))
						{
							//ALGO***************************************************************
						}
						else if(mCallName.equals("printStr"))
						{
							//ALGO***************************************************************
						}
						else if(mCallName.equals("readInt"))
						{
							//ALGO***************************************************************
						}
						else
						{
							for(int z = 0; z < methodsList.size(); z++)
							{
								if(mCallName.equals(methodsList.get(z).getChild(1).getText()))
								{
									boolTempMCall = true;
									mCallTree = methodsList.get(z);
									break;
								}
							}
							if(boolTempMCall)
							{
								if((mCallTree.getChildCount() - 3) == (statementList.get(y).getChild(0).getChildCount() - 1))
								{
									for(int z = 2; z < (mCallTree.getChildCount() - 1); z++)
									{
										if(checkExpMaster(statementList.get(y).getChild(0).getChild(z-1)))
										{
											if(!(mCallTree.getChild(z).getText().equals(expTypeGlobal)))
											{
												principalBool = false;
												errores.erroresSemantic(10, mCallName);
												System.exit(0);
												break;
											}
										}
										else
										{
											principalBool = false;
											errores.erroresSemantic(9, mCallName);
											System.exit(0);
											break;
										}
									}
								}
								else
								{
									principalBool = false;
									errores.erroresSemantic(8, mCallName);
									System.exit(0);
								}
							}
							else
							{
								principalBool = false;
								errores.erroresSemantic(7, mCallName);
								System.exit(0);
							}
						}
					}
					else if(statementIni.equals("return"))
					{
						if(statementList.get(y).getChildCount() == 2)
						{
							//Metodos "void" solo pueden ser "return;"
							if(methodsList.get(x).getChild(0).getText().equals("void"))
							{
								principalBool = false;
								errores.erroresSemantic(13, "");
								System.exit(0);
							}
							else if(checkExpMaster(statementList.get(y).getChild(1)))
							{
								if(!(methodsList.get(x).getChild(0).getText().equals(expTypeGlobal)))
								{
									principalBool = false;
									errores.erroresSemantic(12, "");
									System.exit(0);
								}
							}
							else
							{
								principalBool = false;
								errores.erroresSemantic(11, "");
								System.exit(0);
							}
						}
					}
					else if(statementIni.equals("break"))
					{
						//No hay que hacer nada
					}
					else if(statementIni.equals("continue"))
					{
						//No hay que hacer nada
					}
					//LOCATION ->^(STATEMENT location assign_op expr)
					else if((statementList.get(y).getChildCount() == 3) && statementList.get(y).getChild(2).getText().equals("EXP"))
					{
						String varLocationTemp = statementList.get(y).getChild(0).getText();
						String varLocationTypeTemp = "";
						if(statementList.get(y).getChild(0).getChildCount() == 0)//Es una variable (int ó boolean)
						{
							boolean boolTempLocation = false;
							for(int z = 0; z < hashTableList.size(); z++)
							{
								if(hashTableList.get(z).containsKey(varLocationTemp))
								{
									boolTempLocation = true;
									varLocationTypeTemp = hashTableList.get(z).get(varLocationTemp).getType();
									break;
								}
							}
							if(boolTempLocation == false)
							{
								if(tabla.containsKey(statementList.get(y).getChild(0).getText()))
								{
									boolTempLocation = true;
									varLocationTypeTemp = tabla.get(varLocationTemp).getType();
								}
							}
							if(boolTempLocation == true)
							{
								if(statementList.get(y).getChild(2).getChild(0).getText().equals("METHOD_CALL"))
								{
									boolean boolTempMCall = false;
									String mCallName = statementList.get(y).getChild(2).getChild(0).getChild(0).getText();
									Tree mCallTree = null;
									if(mCallName.equals("callout"))
									{
										//ALGO***************************************************************
									}
									else if(mCallName.equals("printInt"))
									{
										//ALGO***************************************************************
									}
									else if(mCallName.equals("printBoolean"))
									{
										//ALGO***************************************************************
									}
									else if(mCallName.equals("printStr"))
									{
										//ALGO***************************************************************
									}
									else if(mCallName.equals("readInt"))
									{
										//ALGO***************************************************************
									}
									else
									{
										for(int z = 0; z < methodsList.size(); z++)
										{
											if(mCallName.equals(methodsList.get(z).getChild(1).getText()))
											{
												boolTempMCall = true;
												mCallTree = methodsList.get(z);
												break;
											}
										}
										if(boolTempMCall)
										{
											if((mCallTree.getChildCount() - 3) == (statementList.get(y).getChild(2).getChild(0).getChildCount() - 1))
											{
												for(int z = 2; z < (mCallTree.getChildCount() - 1); z++)
												{
													if(checkExpMaster(statementList.get(y).getChild(2).getChild(0).getChild(z-1)))
													{
														if(!(mCallTree.getChild(z).getText().equals(expTypeGlobal)))
														{
															principalBool = false;
															errores.erroresSemantic(10, mCallName);
															System.exit(0);
															break;
														}
													}
													else
													{
														principalBool = false;
														errores.erroresSemantic(9, mCallName);
														System.exit(0);
														break;
													}
												}
											}
											else
											{
												principalBool = false;
												errores.erroresSemantic(8, mCallName);
												System.exit(0);
											}
										}
										else
										{
											principalBool = false;
											errores.erroresSemantic(7, mCallName);
											System.exit(0);
										}
									}
								}
								else
								{
									if(checkExpMaster(statementList.get(y).getChild(2)))
									{
										if(!(varLocationTypeTemp.equals(expTypeGlobal)))
										{
											principalBool = false;
											errores.erroresSemantic(14, varLocationTemp);
											System.exit(0);
										}
									}
									else
									{
										principalBool = false;
										errores.erroresSemantic(15, varLocationTemp);
										System.exit(0);
									}
								}
							}
							else
							{
								principalBool = false;
								errores.erroresSemantic(16, varLocationTemp);
								System.exit(0);
							}
						}
						else if(statementList.get(y).getChild(0).getChildCount() == 3)//Es un array (int ó boolean)
						{
							boolean boolTempLocation = false;
							
							for(int z = 0; z < hashTableList.size(); z++)
							{
								if(hashTableList.get(z).containsKey(varLocationTemp))
								{
									boolTempLocation = true;
									varLocationTypeTemp = hashTableList.get(z).get(varLocationTemp).getType();
									break;
								}
							}
							if(boolTempLocation == false)
							{
								if(tabla.containsKey(statementList.get(y).getChild(0).getText()))
								{
									boolTempLocation = true;
									varLocationTypeTemp = tabla.get(varLocationTemp).getType();
								}	
							}
							if(boolTempLocation == true)
							{
								if(checkExpMaster(statementList.get(y).getChild(2)))
								{
									if(varLocationTypeTemp.equals(expTypeGlobal))
									{
										if(checkExpMaster(statementList.get(y).getChild(0).getChild(1)))
										{
											if(!(expTypeGlobal.equals("int")))
											{
												principalBool = false;
												errores.erroresSemantic(17, varLocationTemp);
												System.exit(0);
											}
										}
										else
										{
											principalBool = false;
											errores.erroresSemantic(17, varLocationTemp);
											System.exit(0);
										}
									}
									else
									{
										principalBool = false;
										errores.erroresSemantic(14, varLocationTemp);
										System.exit(0);
									}
								}
								else
								{
									principalBool = false;
									errores.erroresSemantic(15, varLocationTemp);
									System.exit(0);
								}
							}
							else
							{
								principalBool = false;
								errores.erroresSemantic(16, varLocationTemp);
								System.exit(0);
							}
						}
					}
				}
			}
		}
	}

	public void recursionStatements(LinkedList<Tree> statementList, String keyTemp, String methodsListTypeAux)
	{
		if(principalBool)
		{
			numTableMethodsTemp++;//REVISAR *********************************************
			for(int y = 0; y < statementList.size(); y++)
			{
				String statementIni = statementList.get(y).getChild(0).getText();
				if(statementIni.equals("if") && statementList.get(y).getChild(1).getText().equals("EXP") && statementList.get(y).getChild(2).getText().equals("BLOCK"))
				{
					if(checkExpMaster(statementList.get(y).getChild(1)))
					{
						//Para "if"
						if((statementList.get(y).getChildCount() == 3) || (statementList.get(y).getChildCount() == 5))
						{
							Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
							LinkedList<String> hashTableListKeysAux = new LinkedList<String>();
							LinkedList<Tree> varsListAux = new LinkedList<Tree>();
							LinkedList<Tree> statementListAux = new LinkedList<Tree>();
							for(int z = 0; z < statementList.get(y).getChild(2).getChildCount(); z++)
							{
								if(statementList.get(y).getChild(2).getChild(z).getText().equals("VARS"))
								{
									varsListAux.add(statementList.get(y).getChild(2).getChild(z));
								}
								else if(statementList.get(y).getChild(2).getChild(z).getText().equals("STATEMENT"))
								{
									statementListAux.add(statementList.get(y).getChild(2).getChild(z));
								}
							}
							tablaAux.put(keyTemp + "_if_" + numTableScopes, new SemanticAux("if", "", numTableMethodsTemp, numTableScopes));
							hashTableListKeysAux.add(keyTemp + "_if_" + numTableScopes);
							numTableScopes++;
							//Llenando tabla auxiliar de simbolos, parametros vacios.
							for(int k = 0; k < varsListAux.size(); k++)
							{
								String typeTempOther = varsListAux.get(k).getChild(0).getText();
								for(int z = 1; z < varsListAux.get(k).getChildCount(); z++)
								{
									String keyTempOther = varsListAux.get(k).getChild(z).getText();
									//Chequeo unicidad (que no se repita ningun key)
									if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
									{
										principalBool = false;
										errores.erroresSemantic(3, keyTempOther);
										System.exit(0);
										break;
									}
									else
									{
										tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
										hashTableListKeysAux.add(keyTempOther);
										offset+=4;
									}
								}
							}
							hashTableListKeys.add(hashTableListKeysAux);
							hashTableList.add(tablaAux);
							//RECURSION
							recursionStatements(statementListAux, keyTemp + "_if_" + (numTableScopes - 1), methodsListTypeAux);
							//Para "else" (del "if" anterior)
							if(statementList.get(y).getChildCount() == 5)
							{
								Hashtable<String, SemanticAux> tablaAuxOther = new Hashtable<String, SemanticAux>();
								LinkedList<String> hashTableListKeysAuxOther = new LinkedList<String>();
								LinkedList<Tree> varsListAuxOther = new LinkedList<Tree>();
								LinkedList<Tree> statementListAuxOther = new LinkedList<Tree>();
								for(int z = 0; z < statementList.get(y).getChild(4).getChildCount(); z++)
								{
									if(statementList.get(y).getChild(4).getChild(z).getText().equals("VARS"))
									{
										varsListAuxOther.add(statementList.get(y).getChild(4).getChild(z));
									}
									else if(statementList.get(y).getChild(4).getChild(z).getText().equals("STATEMENT"))
									{
										statementListAuxOther.add(statementList.get(y).getChild(4).getChild(z));
									}
								}
								tablaAuxOther.put(keyTemp + "_else_" + numTableScopes, new SemanticAux("else", "", numTableMethodsTemp, numTableScopes));
								hashTableListKeysAuxOther.add(keyTemp + "_else_" + numTableScopes);
								numTableScopes++;
								//Llenando tabla auxiliar de simbolos, parametros vacios.
								for(int k = 0; k < varsListAuxOther.size(); k++)
								{
									String typeTempOther = varsListAuxOther.get(k).getChild(0).getText();
									for(int z = 1; z < varsListAuxOther.get(k).getChildCount(); z++)
									{
										String keyTempOther = varsListAuxOther.get(k).getChild(z).getText();
										//Chequeo unicidad (que no se repita ningun key)
										if(tabla.containsKey(keyTempOther) || tablaAuxOther.containsKey(keyTempOther))
										{
											principalBool = false;
											errores.erroresSemantic(3, keyTempOther);
											System.exit(0);
											break;
										}
										else
										{
											tablaAuxOther.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
											hashTableListKeysAuxOther.add(keyTempOther);
											offset+=4;
										}
									}
								}
								hashTableListKeys.add(hashTableListKeysAuxOther);
								hashTableList.add(tablaAuxOther);
								//RECURSION
								recursionStatements(statementListAuxOther, keyTemp + "_else_" + (numTableScopes - 1), methodsListTypeAux);
							}
							
						}
						else
						{
							principalBool = false;
						}
					}
					else
					{
						principalBool = false;
					}
					
				}
				else if(statementIni.equals("for") && statementList.get(y).getChild(3).getText().equals("EXP") && statementList.get(y).getChild(4).getText().equals("EXP"))
				{
					boolean boolTempFor = false;
					for(int z = 0; z < hashTableList.size(); z++)
					{
						if(hashTableList.get(z).containsKey(statementList.get(y).getChild(1).getText()))
						{
							boolTempFor = true;
							break;
						}
					}
					if(boolTempFor == false)
					{
						if(tabla.containsKey(statementList.get(y).getChild(1).getText()))
						{
							boolTempFor = true;
						}
					}
					if(checkExpMaster(statementList.get(y).getChild(3)) && checkExpMaster(statementList.get(y).getChild(4)))
					{
						Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
						LinkedList<String> hashTableListKeysAux = new LinkedList<String>();
						LinkedList<Tree> varsListAux = new LinkedList<Tree>();
						LinkedList<Tree> statementListAux = new LinkedList<Tree>();
						for(int z = 0; z < statementList.get(y).getChild(5).getChildCount(); z++)
						{
							if(statementList.get(y).getChild(5).getChild(z).getText().equals("VARS"))
							{
								varsListAux.add(statementList.get(y).getChild(5).getChild(z));
							}
							else if(statementList.get(y).getChild(5).getChild(z).getText().equals("STATEMENT"))
							{
								statementListAux.add(statementList.get(y).getChild(5).getChild(z));
							}
						}
						tablaAux.put(keyTemp + "_for_" + numTableScopes, new SemanticAux("for", "", numTableMethodsTemp, numTableScopes));
						hashTableListKeysAux.add(keyTemp + "_for_" + numTableScopes);
						numTableScopes++;
						//Si variable de inicializacion de for no existe, la creamos. Si ya existe, guardar valor original en otro lugar temporalmente y restaurar al finalizar for.
						if(boolTempFor == false)
						{
							tablaAux.put(statementList.get(y).getChild(1).getText(), new SemanticAux("int", 0, offset, numTableScopes));
							hashTableListKeysAux.add(statementList.get(y).getChild(1).getText());
							offset+=4;
						}
						else
						{
							//Guardar valor original en otro lugar temporalmente y restaurarlo al finalizar for.
						}
						//Llenando tabla auxiliar de simbolos, parametros vacios.
						for(int k = 0; k < varsListAux.size(); k++)
						{
							String typeTempOther = varsListAux.get(k).getChild(0).getText();
							for(int z = 1; z < varsListAux.get(k).getChildCount(); z++)
							{
								String keyTempOther = varsListAux.get(k).getChild(z).getText();
								//Chequeo unicidad (que no se repita ningun key)
								if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
								{
									principalBool = false;
									errores.erroresSemantic(3, keyTempOther);
									System.exit(0);
									break;
								}
								else
								{
									tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
									hashTableListKeysAux.add(keyTempOther);
									offset+=4;
								}
							}
						}
						hashTableListKeys.add(hashTableListKeysAux);
						hashTableList.add(tablaAux);
						//RECURSION
						recursionStatements(statementListAux, keyTemp + "_for_" + (numTableScopes - 1), methodsListTypeAux);
					}
					else
					{
						principalBool = false;
						errores.erroresSemantic(6, statementList.get(y).getChild(1).getText());
						System.exit(0);
					}
				}
				else if(statementIni.equals("BLOCK"))
				{
					Hashtable<String, SemanticAux> tablaAux = new Hashtable<String, SemanticAux>();
					LinkedList<String> hashTableListKeysAux = new LinkedList<String>();
					LinkedList<Tree> varsListAux = new LinkedList<Tree>();
					LinkedList<Tree> statementListAux = new LinkedList<Tree>();
					for(int z = 0; z < statementList.get(y).getChild(0).getChildCount(); z++)
					{
						if(statementList.get(y).getChild(0).getChild(z).getText().equals("VARS"))
						{
							varsListAux.add(statementList.get(y).getChild(0).getChild(z));
						}
						else if(statementList.get(y).getChild(0).getChild(z).getText().equals("STATEMENT"))
						{
							statementListAux.add(statementList.get(y).getChild(0).getChild(z));
						}
					}
					tablaAux.put(keyTemp + "_block_" + numTableScopes, new SemanticAux("block", "", numTableMethodsTemp, numTableScopes));
					hashTableListKeysAux.add(keyTemp + "_block_" + numTableScopes);
					numTableScopes++;
					//Llenando tabla auxiliar de simbolos, parametros vacios.
					for(int k = 0; k < varsListAux.size(); k++)
					{
						String typeTempOther = varsListAux.get(k).getChild(0).getText();
						for(int z = 1; z < varsListAux.get(k).getChildCount(); z++)
						{
							String keyTempOther = varsListAux.get(k).getChild(z).getText();
							//Chequeo unicidad (que no se repita ningun key)
							if(tabla.containsKey(keyTempOther) || tablaAux.containsKey(keyTempOther))
							{
								principalBool = false;
								errores.erroresSemantic(3, keyTempOther);
								System.exit(0);
								break;
							}
							else
							{
								tablaAux.put(keyTempOther, new SemanticAux(typeTempOther, 0, offset, numTableScopes));
								hashTableListKeysAux.add(keyTempOther);
								offset+=4;
							}
						}
					}
					hashTableListKeys.add(hashTableListKeysAux);
					hashTableList.add(tablaAux);
					//RECURSION
					recursionStatements(statementListAux, keyTemp + "_block_" + (numTableScopes - 1), methodsListTypeAux);
				}
				else if(statementIni.equals("METHOD_CALL"))
				{
					boolean boolTempMCall = false;
					String mCallName = statementList.get(y).getChild(0).getChild(0).getText();
					Tree mCallTree = null;
					if(mCallName.equals("callout"))
					{
						//ALGO***************************************************************
					}
					else if(mCallName.equals("printInt"))
					{
						//ALGO***************************************************************
					}
					else if(mCallName.equals("printBoolean"))
					{
						//ALGO***************************************************************
					}
					else if(mCallName.equals("printStr"))
					{
						//ALGO***************************************************************
					}
					else if(mCallName.equals("readInt"))
					{
						//ALGO***************************************************************
					}
					else
					{
						for(int z = 0; z < methodsList.size(); z++)
						{
							if(mCallName.equals(methodsList.get(z).getChild(1).getText()))
							{
								boolTempMCall = true;
								mCallTree = methodsList.get(z);
								break;
							}
						}
						if(boolTempMCall)
						{
							if((mCallTree.getChildCount() - 3) == (statementList.get(y).getChild(0).getChildCount() - 1))
							{
								for(int z = 2; z < (mCallTree.getChildCount() - 1); z++)
								{
									if(checkExpMaster(statementList.get(y).getChild(0).getChild(z-1)))
									{
										if(!(mCallTree.getChild(z).getText().equals(expTypeGlobal)))
										{
											principalBool = false;
											errores.erroresSemantic(10, mCallName);
											System.exit(0);
											break;
										}
									}
									else
									{
										principalBool = false;
										errores.erroresSemantic(9, mCallName);
										System.exit(0);
										break;
									}
								}
							}
							else
							{
								principalBool = false;
								errores.erroresSemantic(8, mCallName);
								System.exit(0);
							}
						}
						else
						{
							principalBool = false;
							errores.erroresSemantic(7, mCallName);
							System.exit(0);
						}
					}
				}
				else if(statementIni.equals("return"))
				{
					if(statementList.get(y).getChildCount() == 2)
					{
						//Metodos "void" solo pueden ser "return;"
						if(methodsListTypeAux.equals("void"))
						{
							principalBool = false;
							errores.erroresSemantic(13, "");
							System.exit(0);
						}
						else if(checkExpMaster(statementList.get(y).getChild(1)))
						{
							if(!(methodsListTypeAux.equals(expTypeGlobal)))
							{
								principalBool = false;
								errores.erroresSemantic(12, "");
								System.exit(0);
							}
						}
						else
						{
							principalBool = false;
							errores.erroresSemantic(11, "");
							System.exit(0);
						}
					}
				}
				else if(statementIni.equals("break"))
				{
					//No hay que hacer nada
				}
				else if(statementIni.equals("continue"))
				{
					//No hay que hacer nada
				}
				//LOCATION ->^(STATEMENT location assign_op expr)
				else if((statementList.get(y).getChildCount() == 3) && statementList.get(y).getChild(2).getText().equals("EXP"))
				{
					String varLocationTemp = statementList.get(y).getChild(0).getText();
					String varLocationTypeTemp = "";
					if(statementList.get(y).getChild(0).getChildCount() == 0)//Es una variable (int ó boolean)
					{
						boolean boolTempLocation = false;
						for(int z = 0; z < hashTableList.size(); z++)
						{
							if(hashTableList.get(z).containsKey(varLocationTemp))
							{
								boolTempLocation = true;
								varLocationTypeTemp = hashTableList.get(z).get(varLocationTemp).getType();
								break;
							}
						}
						if(boolTempLocation == false)
						{
							if(tabla.containsKey(statementList.get(y).getChild(0).getText()))
							{
								boolTempLocation = true;
								varLocationTypeTemp = tabla.get(varLocationTemp).getType();
							}
						}
						if(boolTempLocation == true)
						{
							if(statementList.get(y).getChild(2).getChild(0).getText().equals("METHOD_CALL"))
							{
								boolean boolTempMCall = false;
								String mCallName = statementList.get(y).getChild(2).getChild(0).getChild(0).getText();
								Tree mCallTree = null;
								if(mCallName.equals("callout"))
								{
									//ALGO***************************************************************
								}
								else if(mCallName.equals("printInt"))
								{
									//ALGO***************************************************************
								}
								else if(mCallName.equals("printBoolean"))
								{
									//ALGO***************************************************************
								}
								else if(mCallName.equals("printStr"))
								{
									//ALGO***************************************************************
								}
								else if(mCallName.equals("readInt"))
								{
									//ALGO***************************************************************
								}
								else
								{
									for(int z = 0; z < methodsList.size(); z++)
									{
										if(mCallName.equals(methodsList.get(z).getChild(1).getText()))
										{
											boolTempMCall = true;
											mCallTree = methodsList.get(z);
											break;
										}
									}
									if(boolTempMCall)
									{
										if((mCallTree.getChildCount() - 3) == (statementList.get(y).getChild(2).getChild(0).getChildCount() - 1))
										{
											for(int z = 2; z < (mCallTree.getChildCount() - 1); z++)
											{
												if(checkExpMaster(statementList.get(y).getChild(2).getChild(0).getChild(z-1)))
												{
													if(!(mCallTree.getChild(z).getText().equals(expTypeGlobal)))
													{
														principalBool = false;
														errores.erroresSemantic(10, mCallName);
														System.exit(0);
														break;
													}
												}
												else
												{
													principalBool = false;
													errores.erroresSemantic(9, mCallName);
													System.exit(0);
													break;
												}
											}
										}
										else
										{
											principalBool = false;
											errores.erroresSemantic(8, mCallName);
											System.exit(0);
										}
									}
									else
									{
										principalBool = false;
										errores.erroresSemantic(7, mCallName);
										System.exit(0);
									}
								}
							}
							else
							{
								if(checkExpMaster(statementList.get(y).getChild(2)))
								{
									if(!(varLocationTypeTemp.equals(expTypeGlobal)))
									{
										principalBool = false;
										errores.erroresSemantic(14, varLocationTemp);
										System.exit(0);
									}
								}
								else
								{
									principalBool = false;
									errores.erroresSemantic(15, varLocationTemp);
									System.exit(0);
								}
							}
						}
						else
						{
							principalBool = false;
							errores.erroresSemantic(16, varLocationTemp);
							System.exit(0);
						}
					}
					else if(statementList.get(y).getChild(0).getChildCount() == 3)//Es un array (int ó boolean)
					{
						boolean boolTempLocation = false;
						for(int z = 0; z < hashTableList.size(); z++)
						{
							if(hashTableList.get(z).containsKey(varLocationTemp))
							{
								boolTempLocation = true;
								varLocationTypeTemp = hashTableList.get(z).get(varLocationTemp).getType();
								break;
							}
						}
						if(boolTempLocation == false)
						{
							if(tabla.containsKey(statementList.get(y).getChild(0).getText()))
							{
								boolTempLocation = true;
								varLocationTypeTemp = tabla.get(varLocationTemp).getType();
							}	
						}
						if(boolTempLocation == true)
						{
							if(checkExpMaster(statementList.get(y).getChild(2)))
							{
								if(varLocationTypeTemp.equals(expTypeGlobal))
								{
									if(checkExpMaster(statementList.get(y).getChild(0).getChild(1)))
									{
										if(!(expTypeGlobal.equals("int")))
										{
											principalBool = false;
											errores.erroresSemantic(17, varLocationTemp);
											System.exit(0);
										}
									}
									else
									{
										principalBool = false;
										errores.erroresSemantic(17, varLocationTemp);
										System.exit(0);
									}
								}
								else
								{
									principalBool = false;
									errores.erroresSemantic(14, varLocationTemp);
									System.exit(0);
								}
							}
							else
							{
								principalBool = false;
								errores.erroresSemantic(15, varLocationTemp);
								System.exit(0);
							}
						}
						else
						{
							principalBool = false;
							errores.erroresSemantic(16, varLocationTemp);
							System.exit(0);
						}
					}
				}
			}
		}
	}

	public boolean checkExpMaster(Tree expTreeMaster)
	{
		expTypeGlobal = checkExpType(expTreeMaster);
		if(expTypeGlobal.equals("int") || expTypeGlobal.equals("boolean"))
		{
			if(checkExp(expTreeMaster))
			{
				return true;
			}
			else
			{
				principalBool = false;
				return false;
			}
		}
		else
		{
			principalBool = false;
		}
		return false;
	}

	public boolean checkExp(Tree expTree)
	{
		if(expTree.getChildCount() == 1)
		{
			//Type: LITERAL_BOOL=35, LITERAL_ENTEROS=37, VARIABLES=60
			int nType = expTree.getChild(0).getType();
			String typeTempExp = "";
			if(nType == 35)
			{
				typeTempExp = "boolean";
			}
			else if(nType == 37)
			{
				typeTempExp = "int";
			}
			else if(nType == 60)
			{
				String varNameTemp = expTree.getChild(0).getText();
				for(int x = 0; x < hashTableList.size(); x++)
				{
					if(hashTableList.get(x).containsKey(varNameTemp))
					{
						typeTempExp = hashTableList.get(x).get(varNameTemp).getType();
					}
				}
				//Si llega a este "if", la variable que se busca no esta en ninguna Hashtable de metodos, ifs, fors, etc, entonces solo podria estar en Global Scope
				if(tabla.containsKey(varNameTemp))
				{
					typeTempExp = tabla.get(varNameTemp).getType();
				}
			}
			else
			{
				principalBool = false;
				return false;
			}
			if(typeTempExp.equals(expTypeGlobal))
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		else if((expTree.getChildCount() == 2) || (expTree.getChildCount() == 3))
		{
			if(expTree.getChildCount() == 2)
			{
				//Type: CONDNOT=14, RESTA=55
				int nType = expTree.getChild(0).getType();
				if(((nType == 14) && expTypeGlobal.equals("boolean")) || ((nType == 55) && expTypeGlobal.equals("int")))
				{
					if(checkExp(expTree.getChild(1)))
					{
						return true;
					}
					else
					{
						principalBool = false;
						return false;
					}
				}
				else
				{
					principalBool = false;
					return false;
				}
			}
			else if(expTree.getChildCount() == 3)
			{
				if(checkExp(expTree.getChild(0)))
				{
					if(checkExp(expTree.getChild(2)))
					{
						return true;
					}
					else
					{
						principalBool = false;
						return false;
					}
				}
				else
				{
					principalBool = false;
					return false;
				}
			}
		}
		return false;
	}

	public String checkExpType(Tree expTreeType)
	{
		if(expTreeType.getChildCount() == 1)
		{
			//Type: LITERAL_BOOL=35, LITERAL_ENTEROS=37, VARIABLES=60
			int nType = expTreeType.getChild(0).getType();
			if(nType == 35)
			{
				return "boolean";
			}
			else if(nType == 37)
			{
				return "int";
			}
			else if(nType == 60)
			{
				String varNameTemp = expTreeType.getChild(0).getText();
				for(int x = 0; x < hashTableList.size(); x++)
				{
					if(hashTableList.get(x).containsKey(varNameTemp))
					{
						return hashTableList.get(x).get(varNameTemp).getType();
					}
				}
				//Si llega a este "if", la variable que se busca no esta en ninguna Hashtable de metodos, ifs, fors, etc, entonces solo podria estar en Global Scope
				if(tabla.containsKey(varNameTemp))
				{
					return tabla.get(varNameTemp).getType();
				}
				//Si llega aqui, es porque no existe la variable en ninguna Hashtable ni global ni de metodos, ifs, fors, etc, entonces error.
				principalBool = false;
				return "";
			}
			else
			{
				principalBool = false;
				return "";
			}
		}
		else if((expTreeType.getChildCount() == 2) || (expTreeType.getChildCount() == 3))
		{
			if(expTreeType.getChildCount() == 2)
			{
				//Type: CONDNOT=14, RESTA=55
				int nType = expTreeType.getChild(0).getType();
				if((nType == 14) || (nType == 55))
				{
					return checkExpType(expTreeType.getChild(1));
				}
				else
				{
					principalBool = false;
					return "";
				}
			}
			else if(expTreeType.getChildCount() == 3)
			{
				return checkExpType(expTreeType.getChild(0));
			}
		}
		else
		{
			principalBool = false;
		}
		return "";
	}

	public String getTable()
	{
		String getStringAll = "\n*~*~*~*~*~  Global Scope  ~*~*~*~*~*\n";
		for(int x = 0; x < fieldsList.size(); x++)
		{
			String typeTemp = fieldsList.get(x).getChild(0).getText();
			for(int y = 1; y < fieldsList.get(x).getChildCount(); y++)
			{
				String keyTemp = fieldsList.get(x).getChild(y).getText();
				if(typeTemp.equals("int"))
				{
					getStringAll = getStringAll + keyTemp + " = " + tabla.get(keyTemp).intToString() + "\n";
				}
				else if(typeTemp.equals("boolean"))
				{
					getStringAll = getStringAll + keyTemp + " = " + tabla.get(keyTemp).boolToString() + "\n";
				}
			}
		}

		for(int x = 0; x < arraysList.size(); x++)
		{
			String typeTemp = arraysList.get(x).getChild(0).getText();
			for(int y = 1; y < arraysList.get(x).getChildCount(); y+=2)
			{
				String keyTemp = arraysList.get(x).getChild(y).getText();
				if(typeTemp.equals("int"))
				{
					getStringAll = getStringAll + keyTemp + " = " + tabla.get(keyTemp).intListToString() + "\n";
				}
				else if(typeTemp.equals("boolean"))
				{
					getStringAll = getStringAll + keyTemp + " = " + tabla.get(keyTemp).boolListToString() + "\n";
				}
			}
		}

		for(int x = 0; x < methodsList.size(); x++)
		{
			String typeTemp = methodsList.get(x).getChild(0).getText();
			String keyTemp = methodsList.get(x).getChild(1).getText();
			if(tabla.containsKey(keyTemp))
			{
				getStringAll = getStringAll + keyTemp + " = " + tabla.get(keyTemp).methodParameterToString() + "\n";	
			}
		}
		
		for(int x = 0; x < methodsList.size(); x++)
		{
			String typeTemp = methodsList.get(x).getChild(0).getText();
			String keyTemp = methodsList.get(x).getChild(1).getText();
			getStringAll = getStringAll + "\n\n**********  " + keyTemp + "  **********\n";
			for(int y = 0; y < hashTableListKeys.get(x).size(); y++)
			{
				String keyTempAux = hashTableListKeys.get(x).get(y);
				String typeTempAUx = hashTableList.get(x).get(keyTempAux).getType();
				if(typeTempAUx.equals("int"))
				{
					getStringAll = getStringAll + keyTempAux + " = " + hashTableList.get(x).get(keyTempAux).intToString() + "\n";
				}
				else if(typeTempAUx.equals("boolean"))
				{
					getStringAll = getStringAll + keyTempAux + " = " + hashTableList.get(x).get(keyTempAux).boolToString() + "\n";
				}
			}
			
			getStringAll = getStringAll + "\n";
			
			/*for(int y = 0; y < hashTableListKeys.size(); y++)
			{
				for(int z = 0; z < hashTableListKeys.get(y).size(); z++)
				{
					String keyTempAux = hashTableListKeys.get(y).get(z);
					String typeTempAUx = hashTableList.get(y).get(keyTempAux).getType();
					int parentTableAux = hashTableList.get(y).get(keyTempAux).getParentTable();
					if(parentTableAux == x)
					{
						if(typeTempAUx.equals("if") || typeTempAUx.equals("else") || typeTempAUx.equals("for") || typeTempAUx.equals("block"))
						{
							getStringAll = getStringAll + keyTempAux + " numTable: " + hashTableList.get(y).get(keyTempAux).getNumTable() + " parentTable: " + hashTableList.get(y).get(keyTempAux).getParentTable() + "\n";
						}	
					}
				}
			}*/
		}
		
		for(int x = 0; x < hashTableListKeys.size(); x++)
		{
			for(int y = 0; y < hashTableListKeys.get(x).size(); y++)
			{
				String keyTempAux = hashTableListKeys.get(x).get(y);
				String typeTempAUx = hashTableList.get(x).get(keyTempAux).getType();
				if(typeTempAUx.equals("if") || typeTempAUx.equals("else") || typeTempAUx.equals("for") || typeTempAUx.equals("block"))
				{
					getStringAll = getStringAll + "\n\n-+-+-+-+-+  " + keyTempAux + "  +-+-+-+-+-\n";
					
					int numTableAux = hashTableList.get(x).get(keyTempAux).getNumTable();
					for(int z = 0; z < hashTableListKeys.get(numTableAux).size(); z++)
					{
						String keyTempAuxPrint = hashTableListKeys.get(numTableAux).get(z);
						String typeTempAuxPrint = hashTableList.get(numTableAux).get(keyTempAuxPrint).getType();
						if(typeTempAuxPrint.equals("int"))
						{
							getStringAll = getStringAll + keyTempAuxPrint + " = " + hashTableList.get(numTableAux).get(keyTempAuxPrint).intToString() + "\n";
						}
						else if(typeTempAuxPrint.equals("boolean"))
						{
							getStringAll = getStringAll + keyTempAuxPrint + " = " + hashTableList.get(numTableAux).get(keyTempAuxPrint).boolToString() + "\n";
						}
					}
				}
			}
		}
		return getStringAll;
	}
}
