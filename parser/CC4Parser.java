package compiler.parser;

import compiler.scanner.*;
import compiler.parser.*;
import compiler.lib.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import org.antlr.runtime.*;

public class CC4Parser
{
	public String archivo = "";
	ErrorHandler errores;
	
	public CC4Parser(Scanner scan)
    {
    	this.archivo = scan.archivo;
    	errores = new ErrorHandler();
    }

    public void print(FileWriter fw, PrintWriter pw)
    {
    	pw.println("\nStage: parser");
    	pw.flush();
    	try
	    {
	    	ANTLRFileStream entrada = new ANTLRFileStream(this.archivo);
	    	compiler.scanner.Decaf lexer = new compiler.scanner.Decaf(entrada);
	    	CommonTokenStream tokens = new CommonTokenStream(lexer);
	    	Decaf parser = new Decaf(tokens);
	    	parser.start();
	    	for(int i = parser.lista.size()-1; i >= 0; i--)
	    	{
	    		pw.println(parser.lista.get(i));
    			pw.flush();
	    	}
	    }
	    catch (Exception e)
	    {
	    	
	    	errores.erroresCompiler(1);
	    	System.exit(0);
	    }    	
    }
}
