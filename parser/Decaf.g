parser grammar Decaf;

options
{
	tokenVocab=Decaf;
	output=AST;
}

tokens
{
	PROGRAM;
	VARS;
	METHOD_DECL;
	METHOD_CALL;
	BLOCK;
	STATEMENT;
	EXP;
	FIELD_DEC;
	ARRAY_DEC;
}

@parser::header
{
	package compiler.parser;
	import java.util.Hashtable;
	import java.util.LinkedList;
}

@members
{
  public LinkedList<String> lista = new LinkedList<String>();
  /*
  LinkedList<String> lista= new LinkedList<String>();
  public LinkedList<String> getListParser()
  {
    return lista;
  }
  */
}

start:          	((CLASS PROGRAM LSQUIGLY field_decl* method_decl* RSQUIGLY) ->^(PROGRAM field_decl* method_decl*))
					{lista.add("\t program");};

field_decl:			(
						  ((type VARIABLES (COMA VARIABLES)* PUNTOYCOMA) ->^(FIELD_DEC type VARIABLES+))
						| ((type VARIABLES LBRACKET int_literal RBRACKET (COMA (VARIABLES LBRACKET int_literal RBRACKET))* PUNTOYCOMA) ->^(ARRAY_DEC type (VARIABLES int_literal)+))
					)
					{lista.add("\t field_decl");};

method_decl:		(
						  ((type VARIABLES LPARENTESIS (type VARIABLES (COMA type VARIABLES)*)? RPARENTESIS block) ->^(METHOD_DECL type VARIABLES ^(type VARIABLES)* block))
						| ((VOID VARIABLES LPARENTESIS (type VARIABLES (COMA type VARIABLES)*)? RPARENTESIS block) ->^(METHOD_DECL VOID VARIABLES ^(type VARIABLES)* block))
					)
					{lista.add("\t method_decl");};

block:				((LSQUIGLY (var_decl)* (statement)* RSQUIGLY) ->^(BLOCK var_decl* statement*))
					{lista.add("\t block");};

var_decl:			((type VARIABLES (COMA VARIABLES)* PUNTOYCOMA) ->^(VARS type VARIABLES+))
					{lista.add("\t var_decl");};

type:				(
						  INT 
						| BOOLEAN
					)
					{lista.add("\t type");};

statement:			( 
						  (location assign_op expr PUNTOYCOMA) ->^(STATEMENT location assign_op expr)
						| (method_call PUNTOYCOMA) ->^(STATEMENT method_call)
						| (IF LPARENTESIS expr RPARENTESIS block ((ELSE block)?) ) ->^(STATEMENT IF expr block (ELSE block)?)
						| (FOR (VARIABLES EQUAL expr) COMA expr block) ->^(STATEMENT FOR VARIABLES EQUAL expr expr block)
						| (RETURN (expr)?  PUNTOYCOMA) ->^(STATEMENT RETURN expr?)
						| (BREAK PUNTOYCOMA) ->^(STATEMENT BREAK)
						| (CONTINUE PUNTOYCOMA) ->^(STATEMENT CONTINUE)
						| (block) ->^(STATEMENT block)
					)
					{lista.add("\t statement");};

location:			(
					  (((VARIABLES) => (VARIABLES)) ->^(VARIABLES))
					| (((VARIABLES LBRACKET expr RBRACKET) => (VARIABLES LBRACKET expr RBRACKET)) ->^(VARIABLES LBRACKET expr RBRACKET))
					)
					{lista.add("\t location");};

					
assign_op:			(
						  EQUAL
						| PLUS_EQUAL
						| MINUS_EQUAL
					)
					{lista.add("\t assign_op");};

method_call:		(
						  (method_name LPARENTESIS (expr (COMA expr)*)? RPARENTESIS) ->^(METHOD_CALL method_name (expr)*)
						| (CALLOUT LPARENTESIS LITERAL_STRING (COMA callout_arg)* RPARENTESIS) ->^(METHOD_CALL CALLOUT LITERAL_STRING (callout_arg)*)
					) 
					{lista.add("\t method_call");};

method_name:		(VARIABLES)
					{lista.add("\t method_name");};

expr:				(andexpr -> andexpr)
					(
						  ((EQUAL_EQUAL andexpr) => (EQUAL_EQUAL r=andexpr)) ->^(EXP $expr EQUAL_EQUAL $r)
						| ((NOT_EQUAL andexpr) => (NOT_EQUAL r=andexpr)) ->^(EXP $expr NOT_EQUAL $r)
						| ((REL_MENOR andexpr) => (REL_MENOR r=andexpr)) ->^(EXP $expr REL_MENOR $r)
						| ((REL_MAYOR andexpr) => (REL_MAYOR r=andexpr)) ->^(EXP $expr REL_MAYOR $r)
						| ((REL_MENOREQ andexpr) => (REL_MENOREQ r=andexpr)) ->^(EXP $expr REL_MENOREQ $r)
						| ((REL_MAYOREQ andexpr) => (REL_MAYOREQ r=andexpr)) ->^(EXP $expr REL_MAYOREQ $r)

					)* ;

andexpr:			(addexpr -> addexpr)
					(
						  ((COND_AND addexpr) => (COND_AND r=addexpr))->^(EXP $andexpr COND_AND $r)
						| ((COND_OR addexpr) => (COND_OR r=addexpr)) ->^(EXP $andexpr COND_OR $r)
					)* ;

addexpr:			(multexpr -> multexpr)
					( 
						  ((SUMA  multexpr) => (SUMA r=multexpr)) ->^(EXP $addexpr SUMA $r)
						| ((RESTA  multexpr) => (RESTA r=multexpr)) ->^(EXP $addexpr RESTA $r)
					)* ;
			 
multexpr:			(atom -> atom)
					(
						  ((MULTIPLICACION atom) => (MULTIPLICACION r=atom)) ->^(EXP $multexpr MULTIPLICACION $r)
						| ((DIVISION atom) => (DIVISION r=atom)) ->^(EXP $multexpr DIVISION $r)
					)* ;

atom:				(
						  location ->^(EXP location)
						| method_call ->^(EXP method_call)
						| literal ->^(EXP literal)
						| RESTA expr ->^(EXP RESTA expr)
						| CONDNOT expr ->^(EXP CONDNOT expr)
						| LPARENTESIS expr RPARENTESIS ->^(EXP expr)
					)
					{lista.add("\t expr");};

callout_arg:		(
						  (expr ->^(expr)) 
						| (LITERAL_STRING ->^(LITERAL_STRING))
					)
					{lista.add("\t callout_arg");};

literal:			(
						  int_literal 
						| LITERAL_CHAR 
						| LITERAL_BOOL
					)
					{lista.add("\t literal");};

int_literal:		(
						  LITERAL_ENTEROS  
						| LITERAL_HEXA
					)
					{lista.add("\t int_literal");};
