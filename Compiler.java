import java.io.*;
import java.util.*;

import compiler.ast.Ast;
import compiler.lib.ErrorHandler;
import compiler.codegen.Codegen;
import compiler.irt.Irt;
import compiler.lib.Debug;
import compiler.scanner.Scanner;
import compiler.semantic.Semantic;
import compiler.parser.CC4Parser;
import compiler.opt.ConstantFolding;
import compiler.opt.Algebraic;

public class Compiler
{
    //verifica la existencia del archivo especificado.
    public boolean existenciaArchivo(String nombreArchivo)
    {
        File archivo = new File(nombreArchivo);
        if(archivo.exists() && (!(nombreArchivo.equals("scanner") || nombreArchivo.equals("parser") || nombreArchivo.equals("ast") || nombreArchivo.equals("semantic") || nombreArchivo.equals("irt") || nombreArchivo.equals("codegen"))))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //cuadro ayuda.
    public void ayuda()
    {
        System.out.println("-o <outname>\n \tEscribir el output a <outname>.\n");
        System.out.println("-target <stage>\n Donde <stage> es uno de: scan, parse, ast, semantic, irt, codegen; la compilación debe proceder hasta la etapa indicada. Por ejemplo, si <stage> es scan, una instancia de scan debe ser creada imprimiendo en el archivo de salida \"stage: scanning\". Si es parse una instancia de parser debe ser creada a partir de la instancia de scanner imprimiendo \"stage: parsing\" además del mensaje de scanner y así sucesivamente.\n");
        System.out.println("-opt <opt_stage>\n <opt_stage> es uno de: constant, algebraic; la compilación\n" + "debe hacer solo la optimización que se le pida, y debe imprimir como \n" + "en -target \"optimizing: constant folding\" o \"optimizing: algebraic simplification\".\n");
        System.out.println("-debug <stage>\n Imprimir información de debugging. Debe haber un mensaje\n" + "por cada etapa listada en <stage> de la forma \"Debugging <stage>\"."+ "<stage> tiene las mismas opciones de -target, con la diferencia que se\n" + "pueden \"debuggear\" varias etapas, separandolas con ':' de la forma scan:parse:etc.");
        System.out.println("-h\n Muestra esta ayuda al usuario.\n");
    }

    //main
    public static void main (String args[])
    {
        Compiler compilador = new Compiler();
        FileWriter salida =null;
        ErrorHandler errores = new ErrorHandler();
        LinkedList<String> listaDebugLimpia = new LinkedList<String>();
        LinkedList<String> listaDebug = new LinkedList<String>();
        
        Scanner scanner = null;
        CC4Parser parser = null;
        Ast ast = null;
        Semantic semantic = null;
        Irt irt = null;
        Codegen codegen = null;
        ConstantFolding constant = null;
        Algebraic algebraic = null;
        Debug debug = null;
        

        int n = args.length;
        int boolh = 0, boolo=0, boolox=0, fasesint=-1, nombreIngresadoInt=0, boolNombre=0,flagt=0;
        String nombreIngresado= "", faseStr="", faseDebug="", faseDebugFinal="", optStr="", nombreIngresadoFinal="";
        boolean existeArchivo =false;
        String[] fases = {"scanner", "parser", "ast", "semantic", "irt", "codegen"};

        //verifica ingreso de archivo en el comand line
        for(int i=0; i<n; i++)
        {
            existeArchivo = compilador.existenciaArchivo(args[i]);
            if(existeArchivo)
            {
                if(!(args[i].equals("scanner") || args[i].equals("parser") || args[i].equals("ast") || args[i].equals("semantic") || args[i].equals("irt") || args[i].equals("codegen")))
                {
                    nombreIngresado = args[i];
                    break;
                }
            }
        }

        if(existeArchivo)
        {
            //ciclo que verifica si hay "-o"
            for(int i=0; i<n; i++)
            {
                if(args[i].equals("-o"))
                {
                    boolo=0;
                    boolox=1;
                    break;
                }
            }
            
            for(int x=nombreIngresado.length()-1; x>=0; x--)
            {
                if(nombreIngresado.charAt(x)=='.')
                {
                    nombreIngresadoInt = x;
                    boolNombre=1;
                    break;
                }
                else
                {
                    nombreIngresadoFinal = nombreIngresado;
                }
            }

            if(boolNombre==1)
            {
                nombreIngresadoFinal="";
                for(int j=0; j<=nombreIngresadoInt-1;j++)
                {
                    nombreIngresadoFinal = nombreIngresadoFinal + nombreIngresado.charAt(j);
                }
            }

            if(boolox==0)
            {
                boolo=1;
                try 
                {
                    salida = new FileWriter(nombreIngresadoFinal + ".s");
                } 
                catch (IOException ex) 
                {
                    errores.erroresCompiler(2);
                } 
            }

            //ciclo en donde se trabaja todo.
            for(int i=0;i<n;i++)
            {
                if(args[i].equals("-o") )
                {
                    if(((i+1)<=n) && (args[i+1].charAt(0)!='-'))
                    {
                        if(boolo==0)
                        {
                            boolo=1;
                            try 
                            {
                                nombreIngresadoFinal = args[i+1];
                                salida = new FileWriter(args[i+1] + ".s");
                            } 
                            catch (IOException ex) 
                            {
                                errores.erroresCompiler(2);          
                            }
                        }
                        i++;
                    }
                    else
                    {
                        errores.erroresCompiler(3);
                        System.exit(0);
                    }
                }
                else if(args[i].equals("-h"))
                {
                    if(boolh==0)
                    {
                        boolh = 1;
                        compilador.ayuda();
                    }
                }
                else if(args[i].equals("-target"))
                {
                    if((i+1)<=n)
                    {
                        for(int g=0; g<=5;g++)
                        {
                            if(args[i+1].equals(fases[g]))
                            {
                                flagt=1;
                            }
                        }

                        for(int x=0; x<=5; x++)
                        {
                            if(flagt==1)
                            {
                                if(args[i+1].equals(fases[x]))
                                {
                                    if(fasesint<=x)
                                    {
                                        faseStr=fases[x];
                                        fasesint=x;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                errores.erroresCompiler(3);
                                System.exit(0);
                            }
                        }                        
                    }
                    else
                    {
                        errores.erroresCompiler(3);
                        System.exit(0);
                    }
                    i++;
                }
                else if(args[i].equals("-opt"))
                {
                    //escribir en el archivo "optimizing: constant folding" o "optimizing: algebraic simplification"
                    if((i+1)<=n)
                    {
                        if(args[i+1].equals("constant"))
                        {
                            optStr = args[i+1];
                        }
                        else if(args[i+1].equals("algebraic"))
                        {
                            optStr = args[i+1];
                        }
                        else
                        {
                            errores.erroresCompiler(3);
                            System.exit(0);
                        }
                    }
                    else
                    {
                        errores.erroresCompiler(3);
                        System.exit(0);
                    }
                    i++;
                }
                else if(args[i].equals("-debug"))
                {
                    faseDebug = "";
                    faseDebugFinal="";
                    if((i+1)<=n)
                    {
                        faseDebug = args[i+1];
                        for(int x=0; x<faseDebug.length(); x++)
                        {
                            if(faseDebug.charAt(x)==':')
                            {
                                if(faseDebugFinal.equals("scanner"))
                                {
                                    listaDebug.add(faseDebugFinal);
                                    faseDebugFinal = "";
                                }
                                else if(faseDebugFinal.equals("parser"))
                                {
                                    listaDebug.add(faseDebugFinal);
                                    faseDebugFinal = "";   
                                }    
                                else if(faseDebugFinal.equals("ast"))
                                {
                                    listaDebug.add(faseDebugFinal);
                                    faseDebugFinal = "";   
                                }
                                else if(faseDebugFinal.equals("semantic"))
                                {
                                    listaDebug.add(faseDebugFinal);
                                    faseDebugFinal = "";   
                                }
                                else if(faseDebugFinal.equals("irt"))
                                {
                                    listaDebug.add(faseDebugFinal);
                                    faseDebugFinal = "";   
                                }
                                else if(faseDebugFinal.equals("codegen"))
                                {
                                    listaDebug.add(faseDebugFinal);
                                    faseDebugFinal = "";   
                                }
                                else
                                {
                                    errores.erroresCompiler(3);
                                    System.exit(0);
                                }
                            }
                            else
                            {
                                faseDebugFinal = faseDebugFinal + faseDebug.charAt(x);
                            }
                        }
                        
                        if(faseDebugFinal.equals("scanner"))
                        {
                            listaDebug.add(faseDebugFinal);
                            faseDebugFinal = "";
                        }
                        else if(faseDebugFinal.equals("parser"))
                        {
                            listaDebug.add(faseDebugFinal);
                            faseDebugFinal = "";   
                        }    
                        else if(faseDebugFinal.equals("ast"))
                        {
                            listaDebug.add(faseDebugFinal);
                            faseDebugFinal = "";   
                        }
                        else if(faseDebugFinal.equals("semantic"))
                        {
                            listaDebug.add(faseDebugFinal);
                            faseDebugFinal = "";   
                        }
                        else if(faseDebugFinal.equals("irt"))
                        {
                            listaDebug.add(faseDebugFinal);
                            faseDebugFinal = "";   
                        }
                        else if(faseDebugFinal.equals("codegen"))
                        {
                            listaDebug.add(faseDebugFinal);
                            faseDebugFinal = "";   
                        }
                        else
                        {
                            errores.erroresCompiler(3);
                            System.exit(0);
                        }
                    }
                    else
                    {
                        errores.erroresCompiler(3);
                        System.exit(0);
                    }
                    i++;
                }
                else
                {
                    if(args.length==1)
                    {
                        errores.erroresCompiler(4);
                        System.exit(0);
                    }
                }
            }
			
			try
            {
                FileWriter fw= new FileWriter(nombreIngresadoFinal + ".s",true);
                PrintWriter pw = new PrintWriter(fw);

                //imprimimos target
                for(int y=0; y<=fasesint;y++)
                {
                    if(y==0)
                    {
                        scanner = new Scanner(nombreIngresado);
                        scanner.print(fw,pw);
                    }
                    else if(y==1)
                    {
                        parser = new CC4Parser(scanner);
                        parser.print(fw,pw);
                    }
                    else if(y==2)
                    {
                        ast = new Ast(parser);
                        ast.print(fw,pw);
                    }
                    else if(y==3)
                    {
                        semantic = new Semantic(ast);
                        semantic.print(fw,pw); 
                    }
                    else if(y==4)
                    {
                        irt = new Irt(semantic);
                        irt.print(fw,pw);
                    }
                    else if(y==5)
                    {
                        codegen = new Codegen(irt); 
                        codegen.print(fw,pw);
                    }
                }

                //imprimimos opt
                if(optStr.equals("constant"))
                {
                    constant = new ConstantFolding();
                    constant.print(fw,pw);
                }
                else if(optStr.equals("algebraic"))
                {
                    algebraic = new Algebraic();
                    algebraic.print(fw,pw);
                }
            
            }
            catch(Exception ex)
            {
                errores.erroresCompiler(5);
            }

            //imprimimos debug's
            //como ya creamos objeto de debug abajo, tenemos que crear metodos de impresion a pantalla en la clase debug no imprimirlos como string
            //es decir, mandar a imprimir todo desde la clase debug.
            for(int h=0;h<fases.length;h++)
            {   
                for(int l=0;l<listaDebug.size();l++)
                {
                    if(fases[h].equals(listaDebug.get(l)))
                    {
                        listaDebugLimpia.add(fases[h]);
                        break;
                    }
                }
            }
            //imprimimos Debug
			if(listaDebugLimpia.size() >= 1)
            {
                debug = new Debug(nombreIngresado);
            }

            for(int y=0;y<listaDebugLimpia.size();y++)
            {
                for(int w=0;w<fases.length;w++)
                {
                    if(listaDebugLimpia.get(y).equals(fases[w]))
                    {
                        if(w<=fasesint)
                        {
                            if(listaDebugLimpia.get(y).equals("scanner"))
                            {
                                debug.printScan();
                            }
                            else if(listaDebugLimpia.get(y).equals("parser"))
                            {
                                debug.printParser();
                            }
                            else if(listaDebugLimpia.get(y).equals("ast"))
                            {
                                debug.printAst();
                            }
                            else if(listaDebugLimpia.get(y).equals("semantic"))
                            {
                                debug.printSemantic();
                            }
                            else if(listaDebugLimpia.get(y).equals("irt"))
                            {
                                debug.printIrt();
                            }
                            else if(listaDebugLimpia.get(y).equals("codegen"))
                            {
                                debug.printCodegen();
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if(args.length!=0)
            {
				for(int v=0; v<n; v++)
				{
					if(!(args[v].equals("-h") || args[v].equals("-opt") || args[v].equals("-o") || args[v].equals("-target") || args[v].equals("-debug")))
					{
                        errores.erroresCompiler(1);
                        System.exit(0);
                    }
				}
                
			}
        }
	
        //cuadro de ayuda
        if(n==0 || (n==1 && args[0].equals("-h")))
        {
            compilador.ayuda();
        }
    }
}