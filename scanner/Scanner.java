package compiler.scanner;

import java.io.FileWriter;
import java.io.PrintWriter;
import compiler.scanner.*;
import org.antlr.runtime.*;

public class Scanner
{
	public String archivo = "";
    public static String archivoTemp = "";


    public Scanner(String archivo)
    {
        this.archivo = archivo;
        this.archivoTemp = archivo;
    }
    
    public void print(FileWriter fw, PrintWriter pw) throws Exception
    {
        Decaf lexer = new Decaf(new ANTLRFileStream(this.archivo));
        Decaf lexerCopy = new Decaf(new ANTLRFileStream(this.archivo));
        pw.println("Stage: scanner");
        pw.flush();
        while(lexer.nextToken().getType() != Token.EOF)
        {
            switch(lexerCopy.nextToken().getType())
            {
                case 4:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: BOOLEAN");
                    pw.flush();        
                    break;

                case 5:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: BREAK");
                    pw.flush();        
                    break;

                case 6:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: CALLOUT");
                    pw.flush();        
                    break;
                
                case 7:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: CLASS");
                    pw.flush();        
                    break;
                
                case 8:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COLON");
                    pw.flush();        
                    break;
                
                case 9:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COMA");
                    pw.flush();        
                    break;
                
                case 10:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COMILLA");
                    pw.flush();        
                    break;
                
                case 11:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COMILLAS");
                    pw.flush();        
                    break;
                
                case 12:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COMMENT MULTI-LINE");
                    pw.flush();        
                    break;
                
                case 13:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COMMENT LINE");
                    pw.flush();        
                    break;
                
                case 14:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COND_AND");
                    pw.flush();        
                    break;
                
                case 15:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COND_NOT");
                    pw.flush();        
                    break;
                
                case 16:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COND_OR");
                    pw.flush();        
                    break;
                
                case 17:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: COND_XOR");
                    pw.flush();        
                    break;
                
                case 18:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: CONTINUE");
                    pw.flush();        
                    break;
                
                case 19:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: DIVISION");
                    pw.flush();        
                    break;
                
                case 20:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: ELSE");
                    pw.flush();        
                    break;
                
                case 21:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: EQUAL");
                    pw.flush();        
                    break;
                
                case 22:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: EQUAL_EQUAL");
                    pw.flush();        
                    break;
                
                case 23:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: ERR1_LITERAL_CHAR");
                    pw.flush();        
                    break;
                
                case 24:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: ERR2_LITERAL_CHAR");
                    pw.flush();        
                    break;
                
                case 25:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: ERR3_LITERAL_CHAR");
                    pw.flush();        
                    break;
                
                case 26:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: ERR4_LITERAL_CHAR");
                    pw.flush();        
                    break;
                
                case 27:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: ERR5_LITERAL_CHAR");
                    pw.flush();        
                    break;
                
                case 28:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: EROR_LITERAL_HEXA");
                    pw.flush();        
                    break;
                
                case 30:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: FLOTANTES");
                    pw.flush();        
                    break;
                
                case 31:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: FOR");
                    pw.flush();        
                    break;
                
                case 32:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: IF");
                    pw.flush();        
                    break;
                
                case 33:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: INT");
                    pw.flush();        
                    break;
                
                case 34:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LBRACKET");
                    pw.flush();        
                    break;
                
                case 35:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LITERAL_BOOL");
                    pw.flush();        
                    break;
                
                case 36:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LITERAL_CHAR");
                    pw.flush();        
                    break;
                
                case 37:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LITERAL_ENTEROS");
                    pw.flush();        
                    break;
                
                case 38:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LITERAL_HEXA");
                    pw.flush();        
                    break;

                case 39:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LITERAL_STRING");
                    pw.flush();        
                    break;

                case 40:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LPARENTESIS");
                    pw.flush();        
                    break;
                    
                case 41:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: LSQUIGLY");
                    pw.flush();        
                    break;
                    
                case 42:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: MINUS_EQUAL");
                    pw.flush();        
                    break;
                    
                case 43:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: MULTIPLICACION");
                    pw.flush();        
                    break;
                    
                case 44:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: NOT_EQUAL");
                    pw.flush();        
                    break;
                    
                case 45:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: PLUS_EQUAL");
                    pw.flush();        
                    break;
                    
                case 46:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: PROGRAM");
                    pw.flush();        
                    break;
                    
                case 47:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: PUNTO");
                    pw.flush();        
                    break;
                    
                case 48:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: PUNTO Y COMA");
                    pw.flush();        
                    break;
               
                case 49:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: RBRACKET");
                    pw.flush();        
                    break;
                    
                case 50:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: REL_MAYOR");
                    pw.flush();        
                    break;
                    
                case 51:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: REL_MAYOREQ");
                    pw.flush();        
                    break;
                    
                case 52:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: REL_MENOR");
                    pw.flush();        
                    break;
                    
                case 53:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: REL_MENOREQ");
                    pw.flush();        
                    break;
                    
                case 54:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: REMAINDER");
                    pw.flush();        
                    break;
                    
                case 55:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: RESTA");
                    pw.flush();        
                    break;
                    
                case 56:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: RETURN");
                    pw.flush();        
                    break;
                    
                case 57:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: RPARENTESIS");
                    pw.flush();        
                    break;
                    
                case 58:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: RSQUIGLY");
                    pw.flush();        
                    break;
                    
                case 59:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: SUMA");
                    pw.flush();        
                    break;
                    
                case 60:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: VARIABLES");
                    pw.flush();        
                    break;
                    
                case 61:
                    pw.println("\t Line " + lexer.getLine() + ":" + lexer.getCharPositionInLine() + "\t Found: \'" + lexer.getText() + "\'\t Token: VOID");
                    pw.flush();        
                    break;
            }
        }
    }
}